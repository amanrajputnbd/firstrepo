import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import reportWebVitals from './reportWebVitals'
import 'bootstrap/dist/css/bootstrap.css'
//import FormComponent from './component/DDmainComp'
//import LaptopMainComp from './component/laptopMainComp'
//import LaptopMainComp from './component/laptopMainComp'
//import MainComp from './component/mainPicViewerComp'
//import ProductMainComp from './component/productMainComp'
//import SimpleForm from './component/DDSimpleForm2'
//import SimpleForm from './component/simopleFormValidation2'
import ANSectionB from './component/AN-A5-B'
//import ANSectionA from './component/AN-A5-A'
//import SimpleForm from './component/simpleFormValidation'
//import HomeScreen from './component/homeScreen'
//import HomeScreen from './component/homeScreenStudent'
//import Alert from './component/alert'

ReactDOM.render(
  <React.StrictMode>
    <ANSectionB />
  </React.StrictMode>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
