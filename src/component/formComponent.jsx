import React, { Component } from 'react'
import SimpleForm from './simpleForm'
import StudentForm from './studentForm'
class FormComponent extends Component {
  state = {
    persons: [
      { coursename: 'Javascript', faculty: 'Bill', lectures: 20, students: [] },
      { coursename: 'React', faculty: 'Steve', lectures: 28, students: [] },
    ],
    view: 0,
    editIndex: -1,
    viewCourseIndex: -1,
  }
  handlePerson = (person) => {
    console.log('handle Person', person)
    let s1 = { ...this.state }
    s1.editIndex >= 0
      ? (s1.persons[s1.editIndex] = person)
      : s1.persons.push(person)
    s1.view = 0
    s1.editIndex = -1
    this.setState(s1)
  }

  showForm = () => {
    let s1 = { ...this.state }
    s1.view = 1
    this.setState(s1)
  }

  handleStudent = (student) => {
    console.log(student)
    let s1 = { ...this.state }
    s1.persons[s1.viewCourseIndex].students.push(student)
    s1.view = 2
    this.setState(s1)
  }
  deleteCource = (index) => {
    let s1 = { ...this.state }
    s1.persons.splice(index, 1)
    this.setState(s1)
  }
  editPerson = (index) => {
    let s1 = { ...this.state }
    s1.view = 1
    s1.editIndex = index
    this.setState(s1)
  }

  viewCoursDetails = (index) => {
    let s1 = { ...this.state }
    s1.view = 2
    s1.viewCourseIndex = index
    this.setState(s1)
  }
  showStudentForm = () => {
    let s1 = { ...this.state }
    s1.view = 3
    this.setState(s1)
  }

  showCourseList = () => {
    let s1 = { ...this.state }
    s1.view = 0
    s1.viewCourseIndex = -1
    this.setState(s1)
  }

  showCourseDetails = () => {
    let { persons, view, viewCourseIndex } = this.state
    let { coursename, faculty, lectures, students } = persons[viewCourseIndex]
    return (
      <div className="container">
        <h5>Course Name:{coursename}</h5>
        <h5>Faculty:{faculty}</h5>
        <h5>Number of Lectures:{lectures}</h5>
        <h5>Number of Students:{students}</h5>
        {students.length === 0 ? (
          ''
        ) : (
          <React.Fragment>
            {students.map((st) => (
              <div className="row">
                <div className="col-6 border">{st.id}</div>
                <div className="col-6 border">{st.name}</div>
              </div>
            ))}
          </React.Fragment>
        )}
        {view === 2 ? (
          <button
            className="btn btn-primary"
            onClick={() => this.showStudentForm()}
          >
            Enroll more Student
          </button>
        ) : (
          <StudentForm
            student={{ id: '', name: '' }}
            onSubmit={this.handleStudent}
          />
        )}
        <br />
        <button
          className="btn btn-primary mt-2"
          onClick={() => this.showCourseList()}
        >
          Show list of Courses
        </button>
      </div>
    )
  }

  render() {
    let person = { coursename: '', faculty: '', lectures: '' }
    let { persons, view, editIndex } = this.state
    return view === 0 ? (
      <div className="container">
        {persons.map((p1, index) => (
          <div className="row">
            <div className="col-3 border">{p1.coursename}</div>
            <div className="col-2 border">{p1.faculty}</div>
            <div className="col-2 border">{p1.lectures}</div>
            <div
              className="col-2 border"
              onClick={() => this.viewCoursDetails(index)}
            >
              {p1.students.length}
            </div>
            <div className="col-3 border">
              <button
                className="btn btn-warning btn-sm m-2"
                onClick={() => this.editPerson(index)}
              >
                Edit
              </button>
              <button
                className="btn btn-danger btn-sm m-2"
                onClick={() => this.deleteCource(index)}
              >
                Delete
              </button>
            </div>
          </div>
        ))}
        <button className="btn btn-primary" onClick={() => this.showForm()}>
          Add Course
        </button>
      </div>
    ) : view === 1 ? (
      <SimpleForm
        person={editIndex >= 0 ? persons[editIndex] : person}
        onSubmit={this.handlePerson}
        edit={editIndex >= 0}
      />
    ) : view === 2 ? (
      this.showCourseDetails()
    ) : (
      this.showCourseDetails()
    )
  }
}
export default FormComponent
