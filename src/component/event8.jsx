import { Button } from 'bootstrap'
import React, { Component } from 'react'
class Event extends Component {
  state = {
    products: [
      { name: 'Liril', sales: 0 },
      { name: 'Lux', sales: 0 },
      { name: 'Dove', sales: 22 },
      { name: 'Pears', sales: 25 },
    ],
    totalSales: 0,
    bestSales: 0,
  }
  bestSales = () => {
    const { products } = this.state
    let best = products.reduce(
      (acc, curr) => (curr.sales > acc ? curr.sales : acc),
      0,
    )
    let s1 = { ...this.state }
    s1.bestSales = best
    s1.totalSales = 0
    this.setState(s1)
  }
  totalSales = () => {
    const { products } = this.state
    let total = products.reduce((acc, curr) => acc + curr.sales, 0)
    console.log(total)
    let s1 = { ...this.state }
    s1.totalSales = total
    s1.bestSales = 0
    this.setState(s1)
  }
  ProdectSale = (name) => alert(name + ' Sold')
  ProdectReturn = (name) => alert(name + ' Return')
  render() {
    const { products, totalSales, bestSales } = this.state
    return (
      <React.Fragment>
        <h3>Sales Details</h3>
        <button
          className="btn btn-primary btn-sm m-2"
          onClick={() => this.bestSales()}
        >
          Best sales
        </button>
        <button
          className="btn btn-primary btn-sm m-2"
          onClick={() => this.totalSales()}
        >
          Total sales
        </button>
        <ul>
          {products.map((st) => {
            const { name, sales } = st
            return (
              <li>
                Name:{name},sales:{sales}
                <button
                  className="btn btn-primary btn-sm m-2"
                  onClick={() => this.ProdectSale(name)}
                >
                  sell
                </button>
                {sales > 0 ? (
                  <button
                    className="btn btn-danger btn-sm m-2"
                    onClick={() => this.ProdectReturn(name)}
                  >
                    Return
                  </button>
                ) : (
                  ''
                )}
              </li>
            )
          })}
        </ul>
        {bestSales > 0 ? (
          <React.Fragment>
            <h4>Best Sales = {bestSales}</h4>
          </React.Fragment>
        ) : (
          ''
        )}
        {totalSales > 0 ? (
          <React.Fragment>
            <h4>Total Sales = {totalSales}</h4>
          </React.Fragment>
        ) : (
          ''
        )}
      </React.Fragment>
    )
  }
}
export default Event
