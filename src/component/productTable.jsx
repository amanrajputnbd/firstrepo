import React, { Component } from 'react'
class ProductTable extends Component {
  render() {
    const { products } = this.props
    const { name, code, price, qty } = products
    return (
      <React.Fragment>
        <div className="row bg-light m-1 p-1">
          <div className="col-4 text-center">{name}</div>
          <div className="col-2 text-center">{code}</div>
          <div className="col-2 text-center">{price}</div>
          <div className="col-2 text-center">{qty}</div>
          <div className="col-2 text-center">
            <button className="btn btn-success btn-sm m-1">
              <i className="bi bi-plus"></i>
            </button>
            <button className="btn btn-danger btn-sm m-1">
              <i class="bi bi-dash"></i>
            </button>
          </div>
        </div>
      </React.Fragment>
    )
  }
}
export default ProductTable
