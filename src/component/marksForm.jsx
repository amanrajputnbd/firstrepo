import React, { Component } from 'react'
class MarksForm extends Component {
  state = {
    student: this.props.student,
  }
  handleChange = (e) => {
    let s1 = { ...this.state }
    s1.student[e.currentTarget.name] = e.currentTarget.value
    this.setState(s1)
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.onEnter(this.state.student)
  }

  render() {
    let { maths, eng, comp, science } = this.props.student
    return (
      <div className="container">
        <div className="form-group">
          <label htmlFor="">Maths</label>
          <input
            type="text"
            className="form-control"
            id="maths"
            name="maths"
            value={maths}
            placeholder="Enter the Maths Marks"
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="">English</label>
          <input
            type="text"
            className="form-control"
            id="english"
            name="eng"
            value={eng}
            placeholder="Enter the Marks in English"
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="">Computer</label>
          <input
            type="text"
            className="form-control"
            id="comp"
            name="comp"
            value={comp}
            placeholder="Enter the marks in Computers"
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="">Science</label>
          <input
            type="text"
            className="form-control"
            id="science"
            name="science"
            value={science}
            placeholder="Enter the marks in Science"
            onChange={this.handleChange}
          />
        </div>
        <br />
        <button className="btn btn-primary" onClick={this.handleSubmit}>
          Submit
        </button>
      </div>
    )
  }
}
export default MarksForm
