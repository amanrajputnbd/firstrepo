import React, { Component } from 'react'
class Message extends Component {
  render() {
    const { message1, onBack, onRemove } = this.props
    const { from, to, text, subject, id } = message1
    return (
      <div className="row bg-light">
        <div className="col-11">
          <i class="bi bi-arrow-left" onClick={() => onBack()}></i>
        </div>
        <div className="col">
          <i class="bi bi-trash" onClick={() => onRemove(id)}></i>
        </div>
        <hr />
        <br />
        <div className="col-7">
          From:{from}
          <br />
          To:{to}
          <br />
          Subject:{subject}
          <br />
          {text}
        </div>
      </div>
    )
  }
}
export default Message
