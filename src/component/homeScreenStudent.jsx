import React, { Component } from 'react'
import StudentForm from './studentFormOf'
import MarksForm from './marksForm'
class HomeScreen extends Component {
  state = {
    view: -1,
    enterIndex: -1,
    students: [
      {
        name: 'Jack',
        course: 'B.tech',
        year: 2020,
        maths: '',
        eng: '',
        comp: '',
        science: '',
      },
    ],
  }

  showTable = () => {
    return (
      <React.Fragment>
        <React.Fragment>
          <div className="row bg-dark text-white">
            <div className="col">Name</div>
            <div className="col">Course</div>
            <div className="col">Year</div>
            <div className="col">Total Markes</div>
            <div className="col">Result</div>
          </div>
          {this.state.students.map((c1, index) => {
            let { name, course, year, maths, eng, comp, science } = c1
            let total = +maths + +eng + +comp + +science
            return (
              <div className="row bg-light">
                <div className="col border">{name}</div>
                <div className="col border">{course}</div>
                <div className="col border">{year}</div>
                <div className="col border">
                  {total == 0 || isNaN(total) ? 'No Data' : total}
                </div>
                <div className="col border">
                  {total == 0 || isNaN(total) ? (
                    <button
                      className="btn btn-primary btn-sm"
                      onClick={() => this.handleEnter(index)}
                    >
                      Enter
                    </button>
                  ) : (
                    <button
                      className="btn btn-primary btn-sm"
                      onClick={() => this.handleEdit(index)}
                    >
                      Edit
                    </button>
                  )}
                </div>
              </div>
            )
          })}
        </React.Fragment>
      </React.Fragment>
    )
  }

  handleSubmit = (student) => {
    let s1 = { ...this.state }
    s1.students.push(student)
    s1.view = 2
    this.setState(s1)
  }
  handleEnterMarks = (student) => {
    let s1 = { ...this.state }
    //s1.students.find((m1) => m1.name === student.name)
    s1.view = 2
    this.setState(s1)
  }

  handleNewCust = () => {
    let s1 = { ...this.state }
    s1.view = 1
    this.setState(s1)
  }
  handleList = () => {
    let s1 = { ...this.state }
    s1.view = 2
    this.setState(s1)
  }
  handleEnter = (index) => {
    let s1 = { ...this.state }
    s1.enterIndex = index
    s1.view = 3
    this.setState(s1)
  }
  handleEdit = (index) => {
    let s1 = { ...this.state }
    s1.enterIndex = index
    s1.view = 3
    this.setState(s1)
  }

  render() {
    let { view, enterIndex, students } = this.state
    let student = {
      name: '',
      course: '',
      year: '',
      maths: '',
      eng: '',
      comp: '',
      science: '',
    }
    return (
      <div className="container">
        <button
          className="btn btn-primary m-2"
          onClick={() => this.handleNewCust()}
        >
          New Student
        </button>
        <button
          className="btn btn-primary m-2"
          onClick={() => this.handleList()}
        >
          List of Students
        </button>
        {view === 2 ? (
          this.showTable()
        ) : view === 1 ? (
          <StudentForm student={student} onSubmit={this.handleSubmit} />
        ) : view === 3 ? (
          <MarksForm
            student={enterIndex >= 0 ? students[enterIndex] : student}
            onEnter={this.handleEnterMarks}
          />
        ) : (
          <h4>Welcome to the Student System</h4>
        )}
      </div>
    )
  }
}
export default HomeScreen
