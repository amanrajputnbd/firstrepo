import React, { Component } from 'react'
import Book from './book'
class LibrarySystem extends Component {
  state = {
    books: [
      { name: 'Harry Potter', author: 'Rk Narayan', issued: false },
      { name: 'War and Peace', author: 'Rk Narayan', issued: false },
      { name: 'Malgudi Days', author: 'Rk Narayan', issued: false },
      { name: 'Gitanjli', author: 'Rk Narayan', issued: false },
    ],
  }

  handleIssue = (name) => {
    let s1 = { ...this.state }
    let book = s1.books.find((b1) => b1.name === name)
    if (book) book.issued = true
    this.setState(s1)
  }
  handelReturn = (name) => {
    let s1 = { ...this.state }
    let book = s1.books.find((b1) => b1.name === name)
    if (book) book.issued = false
    this.setState(s1)
  }

  render() {
    let arr = [...this.state.books]
    let books = arr.filter((b1) => b1.issued === false)
    let issueBooks = arr.filter((b1) => b1.issued === true)
    return (
      <div className="container">
        <h4>Books in Library </h4>
        <nav class="navbar navbar-expand-lg navbar-light bg-dark ">
          <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                  <a
                    class="nav-link active text-white"
                    aria-current="page"
                    href="#"
                  >
                    LibrarySystem
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-white" href="#">
                    BOOK IN LIBRARY
                    <span>({arr.length})</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-white">
                    BOOK ISSUE
                    <span>({issueBooks.length})</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <div className="row">
          {books.map((b1, index) => {
            return <Book books={b1} index={index} onIssued={this.handleIssue} />
          })}
        </div>
        <h4>Issued Books</h4>
        <ul>
          {issueBooks.map((b1) => (
            <li>
              {b1.name}
              <button onClick={() => this.handelReturn(b1.name)}>Return</button>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}
export default LibrarySystem
