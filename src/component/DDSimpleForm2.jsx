import React, { Component } from 'react'
class SimpleForm extends Component {
  state = {
    person: {
      name: '',
      age: '',
      country: '',
      gender: '',
      passport: '',
      license: '',
      citiy: '',
      passportNumber: '',
      designations: '',
      techsKnown: [],
      workExp: '',
      manager: '',
      servers: [],
    },
    countries: [
      'United States of America',
      'Canada',
      'India',
      'United Kingdom',
    ],
    countryLists: [
      {
        country: 'United States of America',
        cities: ['New York', 'Los Angeles', 'Seattle', 'San francisco'],
      },
      { country: 'Canada', cities: ['Toronto', 'Montreal', 'Vancouver'] },
      {
        country: 'India',
        cities: ['New Delhi', 'Bengaluru', 'Pune', 'Chennai'],
      },
      {
        country: 'United Kingdom',
        cities: ['London', 'Bristol', 'Manchester'],
      },
    ],
    designations: [
      'Developer',
      'Senior Developer',
      'Team Lead',
      'Architect',
      'Delivery Manager',
    ],
    techs: ['React', 'Javascript', 'JQuery', 'Python'],
    workExpArr: ['Fresher', '0-1 years', '1-3 years', '3-5 years', '5+ years'],
    managersArr: [
      'Meg Smith',
      'Bill Watson',
      'Tim Gates',
      'George Cook',
      'Larry Gomes',
    ],
    serversArr: [
      'Development',
      'Deployment',
      'Alpha Test',
      'Beta Test',
      'BackUp',
    ],
  }

  handleChange = (e) => {
    const { currentTarget: input } = e
    let s1 = { ...this.state }
    input.type === 'checkbox'
      ? input.name === 'techsKnown'
        ? (s1.person.techsKnown = this.updateCBs(
            input.checked,
            input.value,
            s1.person.techsKnown,
          ))
        : input.name === 'servers'
        ? (s1.person.servers = this.updateCBs(
            input.checked,
            input.value,
            s1.person.servers,
          ))
        : (s1.person[input.name] = input.checked)
      : (s1.person[input.name] = input.value)
    if (input.name === 'country') s1.person.citiy = ''
    if (!s1.person.passport) s1.person.passportNumber = ''
    this.setState(s1)
  }

  updateCBs = (checked, value, arr) => {
    if (checked) arr.push(value)
    else {
      let index = arr.findIndex((ele) => ele === value)
      if (index >= 0) arr.splice(index, 1)
    }
    return arr
  }

  handleSubmit = (e) => {
    e.preventDefault()
    //this.props.onSubmit(this.state.person)
  }

  render() {
    let {
      name,
      age,
      country,
      gender,
      passport,
      license,
      citiy,
      passportNumber,
      designation,
      techsKnown,
      workExp,
      manager,
      servers,
    } = this.state.person
    const {
      countries,
      countryLists,
      designations,
      techs,
      workExpArr,
      managersArr,
      serversArr,
    } = this.state
    const cities = country
      ? countryLists.find((c1) => c1.country === country).cities
      : ''

    return (
      <div className="container">
        <h5>Enter Details of Person</h5>
        <div className="form-group">
          <label htmlFor="">Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={name}
            placeholder="Enter the name"
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="">Age</label>
          <input
            type="text"
            className="form-control"
            id="age"
            name="age"
            value={age}
            placeholder="Enter the Age"
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label>Country</label>
          <select
            name="country"
            className="form-control"
            value={country}
            onChange={this.handleChange}
          >
            <option value="" disabled>
              Select the Country
            </option>
            {countries.map((c1) => (
              <option>{c1}</option>
            ))}
          </select>
        </div>
        {country ? (
          <div className="form-group">
            <label>City</label>
            <select
              name="citiy"
              className="form-control"
              value={citiy}
              onChange={this.handleChange}
            >
              <option value="" disabled>
                Select the City
              </option>
              {cities.map((c1) => (
                <option>{c1}</option>
              ))}
            </select>
          </div>
        ) : (
          ''
        )}
        <div className="form-check from-check-inline">
          <input
            className="form-check-input"
            type="radio"
            id="gender"
            name="gender"
            value="Male"
            checked={gender === 'Male'}
            onChange={this.handleChange}
          />
          <label className="form-check-lable">Male</label>
        </div>
        <div className="form-check from-check-inline">
          <input
            className="form-check-input"
            type="radio"
            id="gender"
            name="gender"
            value="Female"
            checked={gender === 'Female'}
            onChange={this.handleChange}
          />
          <label className="form-check-lable">Female</label>
        </div>
        <div className="form-check from-check-inline">
          <input
            className="form-check-input"
            type="checkbox"
            name="passport"
            value={passport}
            checked={passport}
            onChange={this.handleChange}
          />
          <label className="form-check-lable">PassPort</label>
        </div>
        {passport ? (
          <div className="form-group">
            <label htmlFor="">Passport Number</label>
            <input
              type="text"
              className="form-control"
              id="passportNumber"
              name="passportNumber"
              value={passportNumber}
              placeholder="Enter the passport Number"
              onChange={this.handleChange}
            />
          </div>
        ) : (
          ''
        )}
        <div className="form-check from-check-inline">
          <input
            className="form-check-input"
            type="checkbox"
            name="license"
            value={license}
            checked={license}
            onChange={this.handleChange}
          />
          <label className="form-check-lable">License</label>
        </div>
        <label className="form-check-lable font-weight-bold">
          <b>Choose the Designation</b>
        </label>
        {designations.map((d1) => (
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="designation"
              value={d1}
              checked={designation === d1}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{d1}</label>
          </div>
        ))}
        <br />
        <label className="form-check-lable">
          <b>Choose the Technology</b>
        </label>
        {techs.map((t1) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name="techsKnown"
              value={t1}
              checked={techsKnown.findIndex((tech) => tech === t1) >= 0}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{t1}</label>
          </div>
        ))}
        <div className="form-group">
          <label> Select work experience</label>
          <select
            name="workExp"
            className="form-control"
            value={workExp}
            onChange={this.handleChange}
          >
            <option value="" disabled>
              Select the Year
            </option>
            {workExpArr.map((c1) => (
              <option>{c1}</option>
            ))}
          </select>
        </div>
        <label className="form-check-lable">
          <b>Choose the Manager</b>
        </label>
        {managersArr.map((m1) => (
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="manager"
              value={m1}
              checked={manager === m1}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{m1}</label>
          </div>
        ))}
        <label className="form-check-lable">
          <b>choose the servers the person has access to</b>
        </label>
        {serversArr.map((s1) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name="servers"
              value={s1}
              checked={servers.findIndex((ser) => ser === s1) >= 0}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{s1}</label>
          </div>
        ))}
        <br />
        <button className="btn btn-primary" onClick={this.handleSubmit}>
          Submit
        </button>
      </div>
    )
  }
}
export default SimpleForm
