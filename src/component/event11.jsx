import { Button } from 'bootstrap'
import React, { Component } from 'react'
class Event extends Component {
  state = {
    arr: ['A', 'B', 'C', 1, 2, 3],
    code: '',
    newArr: [],
  }
  create = (c) => {
    let s1 = { ...this.state }
    s1.code += c
    this.setState(s1)
  }
  add = () => {
    let s1 = { ...this.state }
    console.log(s1.code)
    let x = s1.newArr.find((st) => st === s1.code)
    if (x) alert('Code already Exist')
    else {
      s1.newArr.push(s1.code)
      s1.code = ''
    }
    this.setState(s1)
  }
  clearCode = () => {
    let s1 = { ...this.state }
    s1.code = ''
    this.setState(s1)
  }
  render() {
    const { arr, code, newArr } = this.state
    return (
      <div className="container">
        <h1>Create New Code</h1>
        <br />
        Code so far:{code}
        <br />
        {arr.map((st) => (
          <button
            className="btn-warning m-2 px-2"
            onClick={() => this.create(st)}
          >
            {st}
          </button>
        ))}
        <br />
        <button className="btn-primary m-3" onClick={() => this.add()}>
          Add New Code
        </button>
        <button className="btn-primary m-3" onClick={() => this.clearCode()}>
          Clear
        </button>
        <br />
        <h3>List of Code Created</h3>
        <br />
        <ul>
          {newArr.map((st) => (
            <li>{st}</li>
          ))}
        </ul>
      </div>
    )
  }
}
export default Event
