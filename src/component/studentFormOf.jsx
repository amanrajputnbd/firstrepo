import React, { Component } from 'react'
class StudentForm extends Component {
  state = {
    student: this.props.student,
    errors: {},
  }
  handleChange = (e) => {
    let { currentTarget: input } = e
    let s1 = { ...this.state }
    s1.student[input.name] = input.value
    this.handleValidate(e)
    this.setState(s1)
  }

  handleSubmit = (e) => {
    e.preventDefault()
    let errors = this.validateAll()
    if (this.isValid(errors)) this.props.onSubmit(this.state.student)
    else {
      let s1 = { ...this.state }
      s1.errors = errors
      this.setState(s1)
    }
  }

  handleValidate = (e) => {
    //only the field which the event has beeb fired
    let { currentTarget: input } = e
    let s1 = { ...this.state }
    switch (input.name) {
      case 'name':
        s1.errors.name = this.validateName(input.value)
        break
      case 'course':
        s1.errors.course = this.validateCourse(input.value)
        break
      case 'year':
        s1.errors.year = this.validateYear(input.value)
        break
      default:
        break
    }
    this.setState(s1)
  }

  isValid = (errors) => {
    //errors would have keys with non empty string as vslue
    let keys = Object.keys(errors)
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0)
    return count === 0
  }

  validateAll = () => {
    let { name, course, year } = this.state.student
    let errors = {}
    errors.name = this.validateName(name)
    errors.course = this.validateCourse(course)
    errors.year = this.validateYear(year)
    return errors
  }

  validateName = (name) =>
    !name
      ? 'Name must be enter'
      : name.length < 5
      ? 'Name should have mininum 5 character'
      : ''

  validateYear = (year) =>
    !year
      ? 'Year must be enter'
      : year < 2010 || year > 2022
      ? 'Year should be between 2010 and 2022'
      : ''

  validateCourse = (course) => (!course ? 'Course must be enter' : '')

  isFormValid = () => {
    let errors = this.validateAll()
    return this.isValid(errors)
  }

  render() {
    let { name, course, year } = this.state.student
    let { errors } = this.state
    return (
      <div className="container">
        <div className="form-group">
          <label htmlFor="">Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={name}
            placeholder="Enter the name"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.name ? (
            <span className="text-danger">{errors.name}</span>
          ) : (
            ''
          )}
        </div>
        <div className="form-group">
          <label htmlFor="">Course</label>
          <input
            type="text"
            className="form-control"
            id="course"
            name="course"
            value={course}
            placeholder="Enter the Course"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.course ? (
            <span className="text-danger">{errors.course}</span>
          ) : (
            ''
          )}
        </div>
        <div className="form-group">
          <label htmlFor="">Year</label>
          <input
            type="text"
            className="form-control"
            id="year"
            name="year"
            value={year}
            placeholder="Enter the Year of Course"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.year ? (
            <span className="text-danger">{errors.year}</span>
          ) : (
            ''
          )}
        </div>
        <br />
        <button
          className="btn btn-primary"
          onClick={this.handleSubmit}
          disabled={!this.isFormValid()}
        >
          Submit
        </button>
      </div>
    )
  }
}
export default StudentForm
