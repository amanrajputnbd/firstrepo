import React, { Component } from 'react'
class MyComp extends Component {
  state = {
    students: [
      { name: 'Williams', maths: 34, english: 36 },
      { name: 'George', maths: 24, english: 31 },
      { name: 'Katherine', maths: 36, english: 41 },
      { name: 'Sophia', maths: 45, english: 37 },
      { name: 'Timothy', maths: 22, english: 19 },
    ],
  }

  getRowStyle = (n1, n2) => (n1 + n2 >= 60 ? 'bg-success' : 'bg-danger')

  render() {
    let { students } = this.state
    const sts = students.filter((st) => st.maths + st.english >= 60)
    return (
      <div className="container">
        <div className="row border bg-dark text-white">
          <div className="col-6 border">Name</div>
          <div className="col-3 border text-center">Maths</div>
          <div className="col-3 border text-center">English</div>
        </div>

        {sts.map((st) => {
          let { name, maths, english } = st
          return (
            <div className={'row border ' + this.getRowStyle(maths, english)}>
              <div className="col-6 border">{name}</div>
              <div className="col-3 border text-center">{maths}</div>
              <div className="col-3 border text-center">{english}</div>
            </div>
          )
        })}
      </div>
    )
  }
}
export default MyComp
