import React, { Component } from 'react'
class Student extends Component {
  state = {
    secA: [
      { roll: 1, sec: 'A', name: 'Jack', maths: 67, eng: 71, comp: 61 },
      { roll: 2, sec: 'A', name: 'Mary', maths: 79, eng: 74, comp: 51 },
      { roll: 3, sec: 'A', name: 'Steve', maths: 61, eng: 78, comp: 46 },
      { roll: 4, sec: 'A', name: 'Bob', maths: 75, eng: 67, comp: 68 },
      { roll: 5, sec: 'A', name: 'Kathy', maths: 70, eng: 63, comp: 74 },
      { roll: 6, sec: 'A', name: 'Meg', maths: 46, eng: 61, comp: 63 },
      { roll: 7, sec: 'A', name: 'Tom', maths: 72, eng: 85, comp: 65 },
      { roll: 8, sec: 'A', name: 'David', maths: 85, eng: 71, comp: 85 },
    ],
    secB: [
      { roll: 1, sec: 'B', name: 'Arthur', maths: 67, eng: 55, comp: 78 },
      { roll: 2, sec: 'B', name: 'Kevin', maths: 69, eng: 64, comp: 68 },
      { roll: 3, sec: 'B', name: 'Harry', maths: 61, eng: 88, comp: 80 },
      { roll: 4, sec: 'B', name: 'Martin', maths: 65, eng: 60, comp: 48 },
      { roll: 5, sec: 'B', name: 'Pam', maths: 80, eng: 53, comp: 54 },
      { roll: 6, sec: 'B', name: 'Nicky', maths: 76, eng: 71, comp: 83 },
      { roll: 7, sec: 'B', name: 'Robert', maths: 82, eng: 65, comp: 75 },
      { roll: 8, sec: 'B', name: 'Susan', maths: 65, eng: 81, comp: 50 },
    ],
    click: -1,
    allSecTotal: [],
    allSecName: [],
  }

  sort = (n) => {
    let s1 = { ...this.state }
    if (n === 0) {
      let arr1 = s1.secA.map((p1) => ({
        ...p1,
        total: p1.maths + p1.eng + p1.comp,
      }))
      s1.secA = arr1.sort((n1, n2) => n2.total - n1.total)
      s1.click = 1
      console.log(s1.secA)
      this.setState(s1)
    } else if (n === 1) {
      let arr1 = s1.secB.map((p1) => ({
        ...p1,
        total: p1.maths + p1.eng + p1.comp,
      }))
      s1.secB = arr1.sort((n1, n2) => n2.total - n1.total)
      s1.click = 2
      this.setState(s1)
    } else if (n === 2) {
      let arr = [...s1.secA, ...s1.secB]
      let arr1 = arr.map((p1) => ({
        ...p1,
        total: p1.maths + p1.eng + p1.comp,
      }))
      s1.allSecTotal = arr1.sort((n1, n2) => n2.total - n1.total)
      s1.click = 3
      this.setState(s1)
    } else if (n === 3) {
      let arr1 = s1.secA.map((p1) => ({
        ...p1,
        total: p1.maths + p1.eng + p1.comp,
      }))
      s1.secA = arr1.sort((n1, n2) => n1.name.localeCompare(n2.name))
      s1.click = 4
      console.log(s1.secA)
      this.setState(s1)
    } else if (n === 4) {
      let arr1 = s1.secB.map((p1) => ({
        ...p1,
        total: p1.maths + p1.eng + p1.comp,
      }))
      s1.secB = arr1.sort((n1, n2) => n1.name.localeCompare(n2.name))
      s1.click = 5
      this.setState(s1)
    } else {
      let arr = [...s1.secA, ...s1.secB]
      let arr1 = arr.map((p1) => ({
        ...p1,
        total: p1.maths + p1.eng + p1.comp,
      }))
      s1.allSecName = arr1.sort((n1, n2) => n1.name.localeCompare(n2.name))
      s1.click = 6
      this.setState(s1)
    }
  }
  showTable = (arr) => {
    return (
      <React.Fragment>
        <div className="row bg-dark text-white">
          <div className="col-1 border">RollNo</div>
          <div className="col-1 border text-center">Section</div>
          <div className="col-2 border text-center">Name</div>
          <div className="col-2 border text-center">Maths</div>
          <div className="col-2 border text-center">English</div>
          <div className="col-2 border text-center">Computers</div>
          <div className="col-2 border text-center">Total</div>
        </div>
        {arr.map((st) => {
          const { name, sec, roll, maths, eng, comp } = st
          return (
            <div className="row ">
              <div className="col-1 border bg-light">{roll}</div>
              <div className="col-1 border text-center bg-light">{sec}</div>
              <div className="col-2 border text-center">{name}</div>
              <div className="col-2 border text-center">{maths}</div>
              <div className="col-2 border text-center">{eng}</div>
              <div className="col-2 border text-center">{comp}</div>
              <div className="col-2 border text-center">
                {maths + comp + eng}
              </div>
            </div>
          )
        })}
      </React.Fragment>
    )
  }

  render() {
    const { secA, secB, click, allSecName, allSecTotal } = this.state
    return (
      <div className="container">
        <button className="btn btn-primary m-2" onClick={() => this.sort(0)}>
          SecA By Total
        </button>
        <button className="btn btn-primary m-2" onClick={() => this.sort(1)}>
          SecB By Total
        </button>
        <button className="btn btn-primary m-2" onClick={() => this.sort(2)}>
          All Sec By Total
        </button>
        <button className="btn btn-primary m-2" onClick={() => this.sort(3)}>
          SecA By Name
        </button>
        <button className="btn btn-primary m-2" onClick={() => this.sort(4)}>
          SecB By Name
        </button>
        <button className="btn btn-primary m-2" onClick={() => this.sort(5)}>
          All Sec By Name
        </button>
        {click === 1
          ? this.showTable(secA)
          : click === 2
          ? this.showTable(secB)
          : click === 3
          ? this.showTable(allSecTotal)
          : click === 4
          ? this.showTable(secA)
          : click === 5
          ? this.showTable(secB)
          : click === 6
          ? this.showTable(allSecName)
          : ''}
      </div>
    )
  }
}
export default Student
