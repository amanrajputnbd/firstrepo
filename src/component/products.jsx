import React, { Component } from 'react'
class Name extends Component {
  state = {
    products: [
      { name: 'Perk', quantity: 10, sales: 7 },
      { name: '5Star', quantity: 7, sales: 9 },
      { name: 'Pepsi', quantity: 10, sales: 20 },
      { name: 'Maggi', quantity: 41, sales: 22 },
      { name: 'Coke', quantity: 18, sales: 50 },
    ],
  }
  getRowStyle = (n1, n2) => (n1 >= n2 ? 'bg-success' : 'bg-danger')
  salesSort = (n1, n2) => n1.sales - n2.sales
  totalQty = () => {
    return this.state.products.reduce((acc, curr) => acc + curr.quantity, 0)
  }
  totalSales = () => {
    return this.state.products.reduce((acc, curr) => acc + curr.sales, 0)
  }
  render() {
    const { products } = this.state
    let products1 = [...products].sort(this.salesSort)
    return (
      <div className="container">
        <h1>Table View</h1>
        <div className="row border bg-dark text-white">
          <div className="col-6 border">Name</div>
          <div className="col-3 border text-center">Quantity</div>
          <div className="col-3 border text-center">Sales</div>
        </div>
        {products1.map((st) => {
          const { name, quantity, sales } = st
          return (
            <div className={'row border ' + this.getRowStyle(sales, quantity)}>
              <div className="col-6 border">{name}</div>
              <div className="col-3 border text-center">{quantity}</div>
              <div className="col-3 border text-center">{sales}</div>
            </div>
          )
        })}
        <div className="row bg-light">
          Total Quantity:{this.totalQty()}
          <br />
          Total Sales:{this.totalSales()}
          <br />
        </div>
      </div>
    )
  }
}
export default Name
