import React, { Component } from 'react'
class ANSectionA extends Component {
  state = {
    products: [
      { product: 'Pepsi', sales: [2, 5, 8, 10, 5] },
      { product: 'Coke', sales: [3, 6, 5, 4, 11, 5] },
      { product: '5Star', sales: [10, 14, 22] },
      { product: 'Maggi', sales: [3, 3, 3, 3, 3] },
      { product: 'Perk', sales: [1, 2, 1, 2, 1, 2] },
      { product: 'Bingo', sales: [0, 1, 0, 3, 2, 6] },
      { product: 'Gems', sales: [3, 3, 1, 1] },
    ],
    detailView: -1,
    sort: -1,
    products2: [],
  }
  render() {
    const { products, detailView, sort, products2 } = this.state
    let view = [products[detailView]]
    return (
      <div className="container">
        <button
          className="btn btn-primary m-2"
          onClick={() => this.sortTable()}
        >
          Short By Product
        </button>
        <button
          className="btn btn-primary m-2"
          onClick={() => this.sortSaleByAsc()}
        >
          Total Sales Asc
        </button>
        <button
          className="btn btn-primary m-2"
          onClick={() => this.sortSaleByDesc()}
        >
          Total Sales Desc
        </button>
        <br />
        {sort < 0
          ? this.showProductTable(products, sort)
          : this.showProductTable(products2, sort)}
        <br />
        {detailView >= 0 ? (
          <ul>
            {view.map((p1) => (
              <li>
                {p1.product}
                <ul>
                  {p1.sales.map((s1) => (
                    <li>{s1}</li>
                  ))}
                </ul>
              </li>
            ))}
          </ul>
        ) : (
          ''
        )}
      </div>
    )
  }

  sortSaleByAsc = () => {
    let s1 = { ...this.state }
    let products = s1.products.map((p1) => {
      let total = p1.sales.reduce((acc, curr) => acc + curr, 0)
      return { product: p1.product, sales: total }
    })
    s1.products2 = products.sort((p1, p2) => p1.sales - p2.sales)
    s1.sort = 1
    this.setState(s1)
  }
  sortSaleByDesc = () => {
    let s1 = { ...this.state }
    let products = s1.products.map((p1) => {
      let total = p1.sales.reduce((acc, curr) => acc + curr, 0)
      return { product: p1.product, sales: total }
    })
    s1.products2 = products.sort((p1, p2) => p2.sales - p1.sales)
    s1.sort = 1
    this.setState(s1)
  }

  sortTable = () => {
    let s1 = { ...this.state }
    s1.products.sort((p1, p2) => p1.product.localeCompare(p2.product))
    s1.sort = -1
    this.setState(s1)
  }

  showProductTable = (products, sort) => {
    if (sort < 0) {
      products = products.map((p1) => {
        let total = p1.sales.reduce((acc, curr) => acc + curr, 0)
        return { product: p1.product, sales: total }
      })
    }
    return (
      <React.Fragment>
        <div className="row bg-dark text-white">
          <div className="col">Product</div>
          <div className="col">Total Sales</div>
          <div className="col"></div>
        </div>
        {products.map((p1, index) => {
          const { product, sales } = p1
          return (
            <div className="row border">
              <div className="col">{product}</div>
              <div className="col">{sales}</div>
              <div className="col">
                <button
                  className="btn btn-primary btn-sm"
                  onClick={() => this.showDetails(index)}
                >
                  Details
                </button>
              </div>
            </div>
          )
        })}
      </React.Fragment>
    )
  }

  showDetails = (index) => {
    let s1 = { ...this.state }
    s1.detailView = index
    this.setState(s1)
  }
}
export default ANSectionA
