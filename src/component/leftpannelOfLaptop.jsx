import React, { Component } from 'react'
class LeftPannel extends Component {
  handleChange = (e) => {
    let s1 = { ...this.props.optionsSel }
    let { currentTarget: input } = e
    input.name === 'brand'
      ? (s1.brand = this.updateCBs(input.checked, input.value, s1.brand))
      : input.name === 'ram'
      ? (s1.ram = this.updateCBs(input.checked, input.value, s1.ram))
      : (s1[input.name] = input.value)
    this.props.onChangeOption(s1)
  }

  updateCBs = (checked, value, arr) => {
    if (checked) arr.push(value)
    else {
      let index = arr.findIndex((ele) => ele === value)
      if (index >= 0) arr.splice(index, 1)
    }
    return arr
  }

  render() {
    const { optionsSel, optionsArray } = this.props
    return (
      <div className="container">
        <h6>Choose Options</h6>
        <button className="btn btn-warning btn-sm" onClick={this.props.ocClear}>
          All Clear
        </button>
        <br />
        {this.showCheckboxes(
          'Brand',
          optionsArray.brand,
          'brand',
          optionsSel.brand,
        )}
        {this.showCheckboxes('Ram', optionsArray.ram, 'ram', optionsSel.ram)}
        {this.showRadios(
          'Processor',
          optionsArray.processor,
          'processor',
          optionsSel.processor,
        )}
        {this.showRadios(
          'Ratings',
          optionsArray.rating,
          'rating',
          optionsSel.rating,
        )}
      </div>
    )
  }

  showCheckboxes = (lable, arr, name, selArr) => {
    return (
      <React.Fragment>
        <label className="form-check-lable">
          <b>{lable}</b>
        </label>
        {arr.map((opt, index) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name={name}
              value={opt}
              checked={selArr.findIndex((sel) => sel === opt) >= 0}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{opt}</label>
          </div>
        ))}
      </React.Fragment>
    )
  }

  showRadios = (lable, arr, name, selVal) => {
    return (
      <React.Fragment>
        {' '}
        <label className="form-check-lable font-weight-bold">
          <b>{lable}</b>
        </label>
        {arr.map((opt) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name={name}
              value={opt}
              checked={selVal === opt}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{opt}</label>
          </div>
        ))}
      </React.Fragment>
    )
  }
}
export default LeftPannel
