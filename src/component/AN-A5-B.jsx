import React, { Component } from 'react'
import { getQuizQuestions } from './quizqns'
import Participants from './quizParticipants'
class ANSectionB extends Component {
  state = {
    players: [
      { name: 'James', point: 0 },
      { name: 'Julia', point: 0 },
      { name: 'Martha', point: 0 },
      { name: 'Steve', point: 0 },
    ],
    questions: getQuizQuestions(),
    currentQuestion: 0,
    playerBuzzer: null,
  }

  handleBuzzer = (index) => {
    let s1 = { ...this.state }
    s1.playerBuzzer = index
    this.setState(s1)
  }

  handleAns = (index, answer) => {
    let s1 = { ...this.state }
    if (index + 1 === answer) {
      s1.players[s1.playerBuzzer].point += 3
      s1.currentQuestion++
      s1.playerBuzzer = null
      alert('Correct Answer. you get 3 points')
    } else {
      s1.players[s1.playerBuzzer].point -= 1
      s1.currentQuestion++
      s1.playerBuzzer = null
      alert('Wrong Answer. you lose 1 point')
    }
    this.setState(s1)
  }

  winnerIs = () => {
    const { players } = this.state
    let maxPoint = players.reduce(
      (acc, curr) => (curr.point > acc ? curr.point : acc),
      0,
    )
    let tiePlayer = players.filter((p1) => p1.point === maxPoint)
    if (tiePlayer.length > 1) {
      let winner = tiePlayer.map((p1) => p1.name)
      return 'There is a tie. The Winners are' + winner.join(',')
    } else {
      let winner = players.find((p1) => p1.point === maxPoint)
      return 'The Winner is ' + winner.name
    }
  }

  render() {
    const { players, currentQuestion, questions, playerBuzzer } = this.state
    return (
      <div className="container">
        <div className="row text-center">
          <h3>Welcome to the Quiz Contest</h3>
          <h6>Participants</h6>
          <div className="row pt-3">
            {players.map((p1, index) => (
              <Participants
                participants={p1}
                onBuzzer={this.handleBuzzer}
                index={index}
                activeBuzzer={playerBuzzer}
              />
            ))}
          </div>
          {questions.map((q1, index) => {
            if (currentQuestion === index) {
              return (
                <div className="row text-center pt-4">
                  <b>Question Number:{index + 1}</b>
                  <p>{q1.text}</p>
                  <br />
                  <div className="col text-center">
                    {q1.options.map((opt, index) => (
                      <button
                        className="btn btn-primary m-1"
                        onClick={() => this.handleAns(index, q1.answer)}
                      >
                        {opt}
                      </button>
                    ))}
                  </div>
                </div>
              )
            }
          })}
          <div className="row text-center pt-4">
            {currentQuestion === questions.length ? (
              <React.Fragment>
                <h4>Game Over</h4>
                <br />
                <h5>{this.winnerIs()}</h5>
              </React.Fragment>
            ) : (
              ''
            )}
          </div>
        </div>
      </div>
    )
  }
}
export default ANSectionB
