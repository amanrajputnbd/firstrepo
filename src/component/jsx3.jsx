import React, { Component, useLayoutEffect } from 'react'
class Name extends Component {
  state = {
    items: [
      { name: 'Liril', stock: 10, sales: 7, price: 10 },
      { name: 'Dove', stock: 20, sales: 9, price: 20 },
      { name: 'Pears', stock: 35, sales: 20, price: 15 },
      { name: 'Surf', stock: 45, sales: 22, price: 55 },
      { name: 'Ariel', stock: 20, sales: 8, price: 40 },
      { name: 'Tide', stock: 20, sales: 11, price: 35 },
      { name: 'Nirma', stock: 30, sales: 21, price: 20 },
    ],
  }
  //sortSalary = (n1, n2) => n1.name.localeCompare(n2.name)
  sortLevel = (n1, n2) => n2.level - n1.level
  itemsTable = () => {
    const { items } = this.state
    //const employees1 = [...employees].sort(this.sortLevel)
    //const employees1 = [...employees].filter((opt) => opt.level > 1)
    return (
      <div className="container">
        <div className="row border bg-dark text-white">
          <div className="col-4 border">Name</div>
          <div className="col-2 text-center">Stock</div>
          <div className="col-2 border text-center">Sales</div>
          <div className="col-2 border text-center">Price</div>
          <div className="col-2 border text-center">Value</div>
        </div>
        {items.map((st) => {
          const { name, stock, sales, price } = st
          return (
            <div className="row ">
              <div className="col-4 border bg-light">{name}</div>
              <div className="col-2 border text-center">{stock}</div>
              <div className="col-2 border text-center">{sales}</div>
              <div className="col-2 border text-center">{price}</div>
              <div className="col-2 border text-center">{price * sales}</div>
            </div>
          )
        })}
        <div className="row bg-light">
          <div className="col-3 border text-center">
            Max Seller by Quantity
            <br />
            Name:Surf
            <br />
            Quantity:22
          </div>
          <div className="col-3 border text-center">
            Max Seller by Value
            <br />
            Name:Surf
            <br />
            Quantity:1210
          </div>
          <div className="col-3 border text-center">
            Min Seller by Quantity
            <br />
            Name:Liril
            <br />
            Quantity:7
          </div>
          <div className="col-3 border text-center">
            Max Seller by Value
            <br />
            Name:Liril
            <br />
            Quantity:70
          </div>
        </div>
        <div className="row bg-light text-center border py-4">
          <div className="col">
            Number of Product:7 <br />
            Total Stock By Quantity:180 <br />
            Total Stock By Value:5600 <br />
            Total Sale By Quantity:98 <br />
            Total Sale By Value:2885
          </div>
        </div>
        <div className="row bg-light text-center border py-2">
          <div className="col">
            <button className="btn btn-primary">
              Close the shope for the Day
            </button>
          </div>
        </div>
      </div>
    )
  }
  render() {
    return this.itemsTable()
  }
}
export default Name
