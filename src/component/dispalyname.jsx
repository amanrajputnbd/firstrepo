import React, { Component } from 'react'
class Name extends Component {
  state = {
    students: [
      { name: 'Bill Johnson', english: 61, maths: 55, computer: 60 },
      { name: 'Mary Smith', english: 75, maths: 80, computer: 82 },
      { name: 'Rex Williams', english: 37, maths: 32, computer: 96 },
    ],
  }
  grad = (maths, english, computer) => {
    let total = maths + english + computer
    if (total >= 225) return 'A'
    else if (total >= 180) return 'B'
    else if (total >= 150) return 'C'
    else return 'D'
  }
  render() {
    const { students } = this.state
    return (
      <div className="container">
        <div className="row ">
          {students.map((st) => {
            let { name, english, maths, computer } = st
            return (
              <div className="col-4">
                Name:{name}
                <br />
                Marks in English:{english}
                <br />
                Marks in Maths:{maths}
                <br />
                Marks in Computer:{computer}
                <br />
                Total Marks:{english + maths + computer}
                <br />
                Grad:{this.grad(maths, english, computer)}
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}
export default Name
