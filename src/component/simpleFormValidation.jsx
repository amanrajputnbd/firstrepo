import React, { Component } from 'react'
class SimpleForm extends Component {
  state = {
    persons: {
      name: '',
      age: '',
      email: '',
      city: '',
      address: '',
    },
    errors: {},
  }

  handleChange = (e) => {
    let { currentTarget: input } = e
    let s1 = { ...this.state }
    s1.persons[input.name] = input.value
    this.handleValidate(e)
    this.setState(s1)
  }

  isValid = (errors) => {
    //errors would have keys with non empty string as vslue
    let keys = Object.keys(errors)
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0)
    return count === 0
  }

  handleSubmit = (e) => {
    //e.preventDefault()
    let errors = this.validateAll()
    if (this.isValid(errors)) alert('Successfully Submitted')
    else {
      let s1 = { ...this.state }
      s1.errors = errors
      this.setState(s1)
    }
  }

  handleValidate = (e) => {
    //only the field which the event has beeb fired
    let { currentTarget: input } = e
    let s1 = { ...this.state }
    switch (input.name) {
      case 'name':
        s1.errors.name = this.validateName(input.value)
        break
      case 'age':
        s1.errors.age = this.validateAge(input.value)
        break
      case 'email':
        s1.errors.email = this.validateEmail(input.value)
        break
      case 'city':
        s1.errors.city = this.validateCity(input.value)
        break
      default:
        break
    }
    this.setState(s1)
  }

  isFormValid = () => {
    let errors = this.validateAll()
    return this.isValid(errors)
  }

  validateAll = () => {
    let { name, age, email, city } = this.state.persons
    let errors = {}
    errors.name = this.validateName(name)
    errors.age = this.validateAge(age)
    errors.email = this.validateEmail(email)
    errors.city = this.validateCity(city)
    //errors.address = this.validateAddress(address)
    return errors
  }

  validateName = (name) =>
    !name
      ? 'Name must be enter'
      : name.length < 5
      ? 'Name should have mininum 5 character'
      : ''

  validateCity = (city) =>
    !city
      ? 'city name must be enter'
      : city.length < 3
      ? 'city name should have mininum 3 character'
      : ''

  validateAge = (age) =>
    !age
      ? 'Age must be enter'
      : age < 20
      ? 'Age should be above 20'
      : +age
      ? ''
      : 'Enter the age in Number'

  validateEmail = (email) =>
    !email
      ? 'Email must be enter'
      : email.indexOf('@') > 0
      ? ''
      : 'Enter the email with example@email.com'

  render() {
    let { name, age, email, city, address } = this.state.persons
    let { errors } = this.state
    return (
      <div className="container">
        <div className="form-group">
          <label htmlFor="">Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={name}
            placeholder="Enter the name"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.name ? (
            <span className="text-danger">{errors.name}</span>
          ) : (
            ''
          )}
        </div>
        <div className="form-group">
          <label htmlFor="">Age</label>
          <input
            type="text"
            className="form-control"
            id="age"
            name="age"
            value={age}
            placeholder="Enter the age"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.age ? <span className="text-danger">{errors.age}</span> : ''}
        </div>
        <div className="form-group">
          <label htmlFor="">Email</label>
          <input
            type="text"
            className="form-control"
            id="email"
            name="email"
            value={email}
            placeholder="Enter the Email"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.email ? (
            <span className="text-danger">{errors.email}</span>
          ) : (
            ''
          )}
        </div>
        <div className="form-group">
          <label htmlFor="">City</label>
          <input
            type="text"
            className="form-control"
            id="city"
            name="city"
            value={city}
            placeholder="Enter the city"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.city ? (
            <span className="text-danger">{errors.city}</span>
          ) : (
            ''
          )}
        </div>
        <div className="form-group">
          <label htmlFor="">Address</label>
          <input
            type="text"
            className="form-control"
            id="address"
            name="address"
            value={address}
            placeholder="Enter the address"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
        </div>
        <br />
        <button
          className="btn btn-primary"
          onClick={() => this.handleSubmit()}
          disabled={!this.isFormValid()}
        >
          Submit
        </button>
      </div>
    )
  }
}
export default SimpleForm
