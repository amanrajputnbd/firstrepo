import React, { Component } from 'react'
class Alert extends Component {
  state = {
    details: {
      working: '',
      studying: '',
      companyname: '',
      designation: '',
      collegename: '',
      course: '',
    },
  }

  handleChange = (e) => {
    const { currentTarget: input } = e
    let s1 = { ...this.state }
    input.type === 'checkbox'
      ? (s1.details[input.name] = input.checked)
      : (s1.details[input.name] = input.value)
    this.setState(s1)
  }

  handleClick = (e) => {
    let s1 = { ...this.state }
    alert(
      'Company Name: ' +
        s1.details.companyname +
        ' Designation: ' +
        s1.details.designation +
        ' College Name: ' +
        s1.details.collegename +
        ' Coure: ' +
        s1.details.course,
    )
    this.setState(s1)
  }

  textFeild = (lable, name, value) => {
    return (
      <React.Fragment>
        <div className="form-group">
          <label htmlFor="">{lable}</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name={name}
            value={value}
            placeholder="Enter the name"
            onChange={this.handleChange}
          />
        </div>
      </React.Fragment>
    )
  }

  render() {
    const {
      working,
      studying,
      course,
      collegename,
      companyname,
      designation,
    } = this.state.details

    return (
      <div className="container">
        <div className="form-check from-check-inline">
          <input
            className="form-check-input"
            type="radio"
            name="working"
            value="working"
            checked={working === 'working'}
            onChange={this.handleChange}
          />
          <label className="form-check-lable">Working</label>
        </div>
        <div className="form-check from-check-inline">
          <input
            className="form-check-input"
            type="radio"
            name="studying"
            value="studying"
            checked={studying === 'studying'}
            onChange={this.handleChange}
          />
          <label className="form-check-lable">Studying</label>
        </div>
        {working ? (
          <React.Fragment>
            <h5>Provide Job Details</h5>
            {this.textFeild('Company Name', 'companyname', companyname)}
            {this.textFeild('Designation', 'designation', designation)}
          </React.Fragment>
        ) : (
          ''
        )}
        {studying ? (
          <React.Fragment>
            <h5>Provide Course Details</h5>
            {this.textFeild('College Name', 'collegename', collegename)}
            {this.textFeild('Course', 'course', course)}
          </React.Fragment>
        ) : (
          ''
        )}
        <button className="btn btn-primary" onClick={() => this.handleClick()}>
          Submit
        </button>
      </div>
    )
  }
}
export default Alert
