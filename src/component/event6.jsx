import React, { Component } from 'react'
class Event extends Component {
  state = {
    socreA: 0,
    socreB: 0,
  }

  addA = (n) => {
    let s1 = { ...this.state }
    s1.socreA += n
    this.setState(s1)
  }
  addB = (n) => {
    let s1 = { ...this.state }
    s1.socreB += n
    this.setState(s1)
  }
  cleraSate = () => {
    let s1 = { ...this.state }
    s1.socreB = 0
    s1.socreA = 0
    this.setState(s1)
  }
  render() {
    const { socreA, socreB } = this.state
    return (
      <div className="container bg-light">
        <div className="row text-center">
          <div className="col-6">
            <h6>Team A</h6>
            <h1>{socreA}</h1>
            <button
              className="btn btn-warning my-2"
              onClick={() => this.addA(3)}
            >
              +3 POINTS
            </button>
            <br />
            <button
              className="btn btn-warning my-2"
              onClick={() => this.addA(2)}
            >
              +2 POINTS
            </button>
            <br />
            <button
              className="btn btn-warning my-2"
              onClick={() => this.addA(6)}
            >
              +6 POINTS
            </button>
            <br />
            <button
              className="btn btn-warning my-2 px-2"
              onClick={() => this.addA(1)}
            >
              FREE THROW
            </button>
          </div>
          <div className="col-6">
            <h6>Team B</h6>
            <h1>{socreB}</h1>
            <button
              className="btn btn-warning my-2"
              onClick={() => this.addB(3)}
            >
              +3 POINTS
            </button>
            <br />
            <button
              className="btn btn-warning my-2"
              onClick={() => this.addB(2)}
            >
              +2 POINTS
            </button>
            <br />
            <button
              className="btn btn-warning my-2"
              onClick={() => this.addB(6)}
            >
              +6 POINTS
            </button>
            <br />
            <button
              className="btn btn-warning my-2 px-2"
              onClick={() => this.addB(1)}
            >
              FREE THROW
            </button>
          </div>
        </div>
        <div className="row text-center">
          <div className="col">
            <button
              className="btn btn-warning"
              onClick={() => this.cleraSate()}
            >
              RESET
            </button>
          </div>
        </div>
      </div>
    )
  }
}
export default Event
