import React, { Component } from 'react'
class Event extends Component {
  state = {
    list1: ['Bob', 'Tim', 'Julia', 'Steve', 'Edwards', 'George', 'Jack'],
    list2: [],
  }

  list1Click = (index) => {
    let s1 = { ...this.state }
    let x1 = s1.list1.splice(index, 1)
    s1.list2.push(x1)
    this.setState(s1)
  }
  list2Click = (index) => {
    let s1 = { ...this.state }
    let x1 = s1.list2.splice(index, 1)
    s1.list1.push(x1)
    this.setState(s1)
  }
  render() {
    const { list1, list2 } = this.state
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-4 border">
            <h4>List1</h4>
            {list1.map((st, index) => {
              return (
                <span onClick={() => this.list1Click(index)}>
                  {st}
                  <br />
                </span>
              )
              ;<br />
            })}
          </div>
          <div className="col-4 border">
            <h4>List2</h4>
            {list2.map((st, index) => (
              <React.Fragment>
                {st}
                <button
                  className="btn btn-danger btn-sm"
                  onClick={() => this.list2Click(index)}
                >
                  x
                </button>
                <br />
              </React.Fragment>
            ))}
          </div>
        </div>
      </React.Fragment>
    )
  }
}
export default Event
