import React, { Component } from 'react'
class Event extends Component {
  state = {
    txt: '',
    clickCount: 0,
  }

  onclickBtn = (str) => {
    let s1 = { ...this.state }
    s1.txt = str
    s1.clickCount += 1
    this.setState(s1)
  }

  render() {
    const { txt, clickCount } = this.state
    return (
      <React.Fragment>
        <button
          className="btn btn-primary m-3"
          onClick={() => this.onclickBtn('ABC')}
        >
          ABC
        </button>
        <button
          className="btn btn-primary m-3"
          onClick={() => this.onclickBtn('123')}
        >
          123
        </button>
        <br />
        txt:{txt}
        <br />
        count:{clickCount}
      </React.Fragment>
    )
  }
}
export default Event
