import React, { Component } from 'react'
import PicViewer from './picViewer'
import Favorites from './favorites'
class MainComp extends Component {
  state = {
    pics: [
      'https://images.pexels.com/photos/356378/pexels-photo-356378.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      'https://images.pexels.com/photos/39317/chihuahua-dog-puppy-cute-39317.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      'https://images.pexels.com/photos/1254140/pexels-photo-1254140.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      'https://images.pexels.com/photos/58997/pexels-photo-58997.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      'https://images.pexels.com/photos/33053/dog-young-dog-small-dog-maltese.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      'https://images.pexels.com/photos/374898/pexels-photo-374898.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      'https://images.pexels.com/photos/1490908/pexels-photo-1490908.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      'https://images.pexels.com/photos/551628/pexels-photo-551628.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      'https://images.pexels.com/photos/1629781/pexels-photo-1629781.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      'https://images.pexels.com/photos/545063/pexels-photo-545063.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      'https://images.pexels.com/photos/257540/pexels-photo-257540.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    ],
    favorites: [],
    currentIndex: 0,
  }

  handlePrevious = () => {
    let s1 = { ...this.state }
    s1.currentIndex =
      s1.currentIndex === 0 ? s1.currentIndex : s1.currentIndex - 1
    this.setState(s1)
  }
  handleNext = () => {
    let s1 = { ...this.state }
    s1.currentIndex =
      s1.currentIndex < s1.pics.length - 1
        ? s1.currentIndex + 1
        : s1.currentIndex
    this.setState(s1)
  }

  addFav = (index1) => {
    let s1 = { ...this.state }
    let pic = s1.pics.find((p1, index) => index === index1)
    if (pic) {
      let fav = s1.favorites.find((f1) => f1 === pic)
      if (!fav) s1.favorites.push(pic)
    }
    this.setState(s1)
  }
  handleFav = (index) => {
    let s1 = { ...this.state }
    s1.favorites.splice(index, 1)
    this.setState(s1)
  }

  render() {
    const { pics, currentIndex, favorites } = this.state
    return (
      <div className="container">
        <div className="row border text-center bg-light">
          {pics.map((p1, index) => (
            <PicViewer
              pics={p1}
              index={index}
              currentIndex={currentIndex}
              addFav={this.addFav}
            />
          ))}
        </div>
        <div className="row">
          <div className="col">
            <button
              className="btn btn-primary"
              onClick={() => this.handlePrevious()}
            >
              Previous
            </button>
          </div>
          <div className="col-2">
            <button
              className="btn btn-primary"
              onClick={() => this.handleNext()}
            >
              Next
            </button>
          </div>
        </div>
        <div className="row">
          {favorites.length > 0 ? <h4>My Favorites</h4> : ''}
          {favorites.map((f1, index) => (
            <Favorites favorites={f1} onRemove={this.handleFav} index={index} />
          ))}
        </div>
      </div>
    )
  }
}
export default MainComp
