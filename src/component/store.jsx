import React, { Component } from 'react'
import Product from './product'
import ProductTable from './productTable'
class Store extends Component {
  state = {
    products: [
      { name: 'Coca Cola', code: 'C322', price: 20, qty: 10 },
      { name: '5Star', code: 'F112', price: 15, qty: 0 },
      { name: 'Maggi', code: 'M221', price: 28, qty: 22 },
      { name: 'Pepsi', code: 'P223', price: 20, qty: 18 },
      { name: 'Dairy Milk', code: 'D302', price: 40, qty: 0 },
      { name: 'Mirinda', code: 'M586', price: 25, qty: 8 },
      { name: 'Kitkat', code: 'K895', price: 16, qty: 11 },
      { name: 'Red Bull', code: 'R122', price: 90, qty: 3 },
    ],
    grideview: false,
  }

  handleIncrease = (code) => {
    let s1 = { ...this.state }
    s1.products.find((p1) => (p1.code === code ? p1.qty++ : ''))
    this.setState(s1)
  }
  handleDecrease = (code) => {
    let s1 = { ...this.state }
    s1.products.find((p1) => (p1.code === code ? p1.qty-- : ''))
    this.setState(s1)
  }
  handleQty = () => {
    let s1 = { ...this.state }
    s1.products = s1.products.sort((p1, p2) => p1.qty - p2.qty)
    this.setState(s1)
  }
  handlePrice = () => {
    let s1 = { ...this.state }
    s1.products = s1.products.sort((p1, p2) => p1.price - p2.price)
    this.setState(s1)
  }
  handleView = () => {
    let s1 = { ...this.state }
    if (s1.grideview) {
      s1.grideview = false
    } else {
      s1.grideview = true
    }
    this.setState(s1)
  }

  render() {
    const { products, grideview } = this.state
    return (
      <div className="container">
        <h2 className="text-center">Product in Store</h2>

        <div className="row text-center">
          <div className="row">
            <div className="col">
              <button
                className="btn btn-primary m-2"
                onClick={() => this.handleQty()}
              >
                Order by Qusntity
              </button>
              <button
                className="btn btn-primary m-2"
                onClick={() => this.handlePrice()}
              >
                Order by price
              </button>
              <button
                className="btn btn-primary m-2"
                onClick={() => this.handleView()}
              >
                View:{grideview === false ? 'Table' : 'Grid'}
              </button>
            </div>
          </div>
          {grideview === true ? (
            <div className="row bg-dark text-white">
              <div className="col-4 text-center">Name</div>
              <div className="col-2 text-center">Code</div>
              <div className="col-2 text-center">Price</div>
              <div className="col-2 text-center">Quantity</div>
              <div className="col-2 text-center"></div>
            </div>
          ) : (
            ''
          )}
          {products.map((p1) => {
            return (
              <React.Fragment>
                {grideview === false ? (
                  <Product
                    products={p1}
                    onIncrease={this.handleIncrease}
                    onDecrease={this.handleDecrease}
                  />
                ) : (
                  <ProductTable products={p1} />
                )}
              </React.Fragment>
            )
          })}
        </div>
      </div>
    )
  }
}
export default Store
