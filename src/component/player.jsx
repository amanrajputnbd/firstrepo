import React, { Component } from 'react'
class Player extends Component {
  render() {
    const { player, onClickPoint, index } = this.props
    const { name, score } = player
    return (
      <div className="row text-primary bg-light border m-1 px-2">
        <div className="col">
          Name:{name}
          <br />
          Score:{score}
          <br />
          <button
            className="btn btn-success btn-sm"
            onClick={() => onClickPoint(index)}
          >
            1 Point
          </button>
        </div>
      </div>
    )
  }
}
export default Player
