import React, { Component } from 'react'
class NavBar extends Component {
  render() {
    let { count, qty } = this.props
    return (
      <nav class="navbar navbar-expand-lg navbar-light bg-dark ">
        <div class="container-fluid">
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a
                  class="nav-link active text-white"
                  aria-current="page"
                  href="#"
                >
                  MainSystem
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white" href="#">
                  Products
                  <span>{count}</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white">
                  Quantity
                  <span>{qty}</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    )
  }
}
export default NavBar
