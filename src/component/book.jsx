import React, { Component } from 'react'
class Book extends Component {
  render() {
    let { books, onIssued } = this.props
    let { name, author } = books
    return (
      <div className="col-4 bg-danger border m-1 text-center">
        {name}
        <br />
        {author}
        <br />
        <button
          className="btn btn-secondary btn-sm"
          onClick={() => onIssued(name)}
        >
          Issue
        </button>
      </div>
    )
  }
}
export default Book
