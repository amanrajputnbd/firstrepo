import React, { Component } from 'react'
import SimpleForm from './DDSimpleForm'
class FormComponent extends Component {
  state = {
    persons: [
      {
        name: 'Williams',
        age: 27,
        country: '',
        gender: 'Male',
        passport: true,
        license: '',
        citiy: '',
        passportNumber: 'FGARXS25522',
        designations: '',
        techsKnown: [],
      },
      {
        name: 'Anna',
        age: 31,
        country: '',
        gender: 'Female',
        passport: false,
        license: '',
        citiy: '',
        passportNumber: '',
        designations: '',
        techsKnown: [],
      },
    ],
    view: 0,
    editIndex: -1,
  }
  handlePerson = (person) => {
    let s1 = { ...this.state }
    s1.editIndex >= 0
      ? (s1.persons[s1.editIndex] = person)
      : s1.persons.push(person)
    s1.view = 0
    s1.editIndex = -1
    this.setState(s1)
  }

  addPerson = () => {
    let s1 = { ...this.state }
    s1.view = 1
    this.setState(s1)
  }

  deletePerson = (index) => {
    let s1 = { ...this.state }
    s1.persons.splice(index, 1)
    this.setState(s1)
  }
  editPerson = (index) => {
    let s1 = { ...this.state }
    s1.view = 1
    s1.editIndex = index
    this.setState(s1)
  }

  viewCoursDetails = (index) => {
    let s1 = { ...this.state }
    s1.view = 2
    s1.viewCourseIndex = index
    this.setState(s1)
  }
  showStudentForm = () => {
    let s1 = { ...this.state }
    s1.view = 3
    this.setState(s1)
  }

  showCourseList = () => {
    let s1 = { ...this.state }
    s1.view = 0
    s1.viewCourseIndex = -1
    this.setState(s1)
  }

  render() {
    let person = {
      name: '',
      age: '',
      country: '',
      gender: '',
      passport: '',
      license: '',
      citiy: '',
      passportNumber: '',
      designations: '',
      techsKnown: [],
    }
    let { persons, view, editIndex } = this.state
    return view === 0 ? (
      <div className="container">
        {persons.map((p1, index) => (
          <div className="row">
            <div className="col-3 border">{p1.name}</div>
            <div className="col-2 border">{p1.age}</div>
            <div className="col-2 border">
              <button
                className="btn btn-warning btn-sm m-2"
                onClick={() => this.editPerson(index)}
              >
                Edit
              </button>
              <button
                className="btn btn-danger btn-sm m-2"
                onClick={() => this.deletePerson(index)}
              >
                Delete
              </button>
            </div>
          </div>
        ))}
        <br />
        <button className="btn btn-primary" onClick={() => this.addPerson()}>
          Add Person
        </button>
      </div>
    ) : (
      <SimpleForm
        person={editIndex >= 0 ? persons[editIndex] : person}
        onSubmit={this.handlePerson}
        edit={editIndex >= 0}
      />
    )
  }
}
export default FormComponent
