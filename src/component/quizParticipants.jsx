import React, { Component } from 'react'
class Participants extends Component {
  palyerColor = (press, index) => {
    if (press === index) return 'bg-success'
    else return 'bg-warning'
  }

  render() {
    const { participants, onBuzzer, index, activeBuzzer } = this.props
    return (
      <div className={'col-3 border  ' + this.palyerColor(activeBuzzer, index)}>
        Name:{participants.name}
        <br />
        score:{participants.point}
        <br />
        <button onClick={() => onBuzzer(index)}>Buzzer</button>
      </div>
    )
  }
}
export default Participants
