import React, { Component } from 'react'
class Keypad extends Component {
  state = {
    upper: [
      'A',
      'B',
      'C',
      'D',
      'E',
      'F',
      'G',
      'H',
      'I',
      'J',
      'K',
      'L',
      'M',
      'N',
      'O',
      'P',
      'Q',
      'R',
      'S',
      'T',
      'U',
      'V',
      'W',
      'X',
      'Y',
      'Z',
    ],
    lower: [
      'a',
      'b',
      'c',
      'd',
      'e',
      'f',
      'g',
      'h',
      'i',
      'j',
      'k',
      'l',
      'm',
      'n',
      'o',
      'p',
      'q',
      'r',
      's',
      't',
      'u',
      'v',
      'w',
      'x',
      'y',
      'z',
    ],
    digits: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    special: ['@', '#', '$', '%', '&,', '*', '(', ')'],
    click: -1,
    text: '',
  }

  upperKeypad = () => {
    let s1 = { ...this.state }
    s1.click = 1
    this.setState(s1)
  }
  digitsKeypad = () => {
    let s1 = { ...this.state }
    s1.click = 2
    this.setState(s1)
  }
  specialKeypad = () => {
    let s1 = { ...this.state }
    s1.click = 3
    this.setState(s1)
  }
  lowerKeypad = () => {
    let s1 = { ...this.state }
    s1.click = 4
    this.setState(s1)
  }

  getvalue = (n) => {
    let s1 = { ...this.state }
    s1.text += n
    this.setState(s1)
  }

  showKeypad = (arr) => {
    return (
      <React.Fragment>
        {arr.map((p1) => (
          <button
            className="btn btn-dark btn-sm m-1"
            onClick={() => this.getvalue(p1)}
          >
            {p1}
          </button>
        ))}
      </React.Fragment>
    )
  }

  render() {
    const { upper, digits, special, click, text, lower } = this.state
    return (
      <div className="container">
        <button
          className="btn btn-primary m-2"
          onClick={() => this.upperKeypad()}
        >
          UpperCase
        </button>
        <button
          className="btn btn-primary m-2"
          onClick={() => this.lowerKeypad()}
        >
          LowerCase
        </button>
        <button
          className="btn btn-primary m-2"
          onClick={() => this.digitsKeypad()}
        >
          Digits
        </button>
        <button
          className="btn btn-primary m-2"
          onClick={() => this.specialKeypad()}
        >
          Special
        </button>
        <h4 className="border">Text:{text}</h4>
        <br />
        {click === 1
          ? this.showKeypad(upper)
          : click === 2
          ? this.showKeypad(digits)
          : click === 3
          ? this.showKeypad(special)
          : click === 4
          ? this.showKeypad(lower)
          : ''}
      </div>
    )
  }
}
export default Keypad
