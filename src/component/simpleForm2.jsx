import React, { Component } from 'react'
class Simple extends Component {
  state = {
    name: '',
    view: -1,
    course: [],
  }
  handleSubmit = () => {
    let s1 = { ...this.state }
    s1.course.push(s1.name)
    s1.view = -1
    s1.name = ''
    this.setState(s1)
  }
  handleForm = () => {
    let s1 = { ...this.state }
    s1.view = 1
    this.setState(s1)
  }
  handleChange = (e) => {
    let s1 = { ...this.state }
    s1.name = e.currentTarget.value
    this.setState(s1)
  }

  render() {
    const { course, view, name } = this.state
    return (
      <div className="container">
        {view <= 0 ? (
          <button className="btn btn-primary" onClick={() => this.handleForm()}>
            Add Course
          </button>
        ) : (
          <React.Fragment>
            <div className="form-group">
              <label htmlFor="">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                name="name"
                value={name}
                placeholder="Enter the coursename"
                onChange={this.handleChange}
              />
            </div>
            <button
              className="btn btn-primary"
              onClick={() => this.handleSubmit()}
            >
              Submit
            </button>
          </React.Fragment>
        )}
        <h4>List of Course</h4>
        {course.length === 0 ? 'There are ZERO Course' : ''}
        <ul>
          {course.map((c1) => (
            <li>
              {c1}
              <button className="btn btn-warning">Edit</button>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}
export default Simple
