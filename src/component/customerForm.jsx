import React, { Component } from 'react'
class CustomerForm extends Component {
  state = {
    customer: this.props.customer,
    deliverOptArr: ['Home', 'Office', 'Pickup'],
    payOptArr: ['Credit Card', 'Debit Card', 'Net Banking'],
    slotArr: ['Before 10 AM', '12pm -2pm', '2pm-6pm'],
  }

  handleChange = (e) => {
    const { currentTarget: input } = e
    let s1 = { ...this.state }
    input.type === 'checkbox'
      ? (s1.customer.payOpt = this.updateCBs(
          input.checked,
          input.value,
          s1.customer.payOpt,
        ))
      : (s1.customer[input.name] = input.value)
    this.setState(s1)
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.onSubmit(this.state.customer)
  }

  updateCBs = (checked, value, arr) => {
    if (checked) arr.push(value)
    else {
      let index = arr.findIndex((ele) => ele === value)
      if (index >= 0) arr.splice(index, 1)
    }
    return arr
  }

  render() {
    let { name, gender, deliver, payOpt, slot } = this.state.customer
    let { deliverOptArr, payOptArr, slotArr } = this.state
    return (
      <div className="container">
        <div className="form-group">
          <label htmlFor="">Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={name}
            placeholder="Enter the name"
            onChange={this.handleChange}
          />
        </div>
        <div className="form-check from-check-inline">
          <input
            className="form-check-input"
            type="radio"
            id="gender"
            name="gender"
            value="Male"
            checked={gender === 'Male'}
            onChange={this.handleChange}
          />
          <label className="form-check-lable">Male</label>
        </div>
        <div className="form-check from-check-inline">
          <input
            className="form-check-input"
            type="radio"
            id="gender"
            name="gender"
            value="Female"
            checked={gender === 'Female'}
            onChange={this.handleChange}
          />
          <label className="form-check-lable">Female</label>
        </div>
        <label className="form-check-lable">
          <b>Choose the Deliver Option</b>
        </label>
        <br />
        {deliverOptArr.map((d1) => (
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="deliver"
              value={d1}
              checked={deliver === d1}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{d1}</label>
          </div>
        ))}
        <br />
        <label className="form-check-lable">
          <b>Choose the Payment Option</b>
        </label>
        {payOptArr.map((p1) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name="payOpt"
              value={p1}
              checked={payOpt.findIndex((sel) => sel === p1) >= 0}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{p1}</label>
          </div>
        ))}
        <div className="form-group">
          <select
            name="slot"
            className="form-control"
            value={slot}
            onChange={this.handleChange}
          >
            <option value="" disabled>
              Select the delivery Slot
            </option>
            {slotArr.map((c1) => (
              <option>{c1}</option>
            ))}
          </select>
        </div>
        <br />
        <button className="btn btn-primary" onClick={this.handleSubmit}>
          Submit
        </button>
      </div>
    )
  }
}
export default CustomerForm
