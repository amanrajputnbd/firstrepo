import React, { Component } from 'react'
import Player from './player'
class PlayerSystem extends Component {
  state = {
    players: [
      { name: 'Jack', score: 21 },
      { name: 'Steve', score: 40 },
      { name: 'Martha', score: 30 },
      { name: 'Bob', score: 33 },
      { name: 'Katherine', score: 22 },
    ],
  }
  handleClick = (index1) => {
    let s1 = { ...this.state }
    let scoreP = s1.players.find((p1, index) => index === index1)
    if (scoreP) scoreP.score++
    this.setState(s1)
  }
  render() {
    const { players } = this.state
    return (
      <div className="container">
        <h4>List of Names</h4>
        {players.map((p1, index) => {
          return (
            <Player player={p1} index={index} onClickPoint={this.handleClick} />
          )
        })}
      </div>
    )
  }
}
export default PlayerSystem
