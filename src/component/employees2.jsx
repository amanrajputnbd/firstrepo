import React, { Component, useLayoutEffect } from 'react'
class Name extends Component {
  state = {
    employees: [
      {
        name: 'Jack Smith',
        level: 2,
        dept: 'Tech',
        designation: 'Manager',
        salary: 24000,
      },
      {
        name: 'Mary Robbins',
        level: 3,
        dept: 'Fin',
        designation: 'Manager',
        salary: 28000,
      },
      {
        name: 'Steve Williams',
        level: 4,
        dept: 'Ops',
        designation: 'President',
        salary: 35000,
      },
      {
        name: 'Bob Andrews',
        level: 1,
        dept: 'Fin',
        designation: 'Trainee',
        salary: 16500,
      },
      {
        name: 'Dave Martin',
        level: 2,
        dept: 'Fin',
        designation: 'Manager',
        salary: 21700,
      },
      {
        name: 'Julia Clarke',
        level: 3,
        dept: 'Ops',
        designation: 'Manager',
        salary: 26900,
      },
      {
        name: 'Kathy Jones',
        level: 4,
        dept: 'Tech',
        designation: 'President',
        salary: 42500,
      },
      {
        name: 'Tom Bresnan',
        level: 2,
        dept: 'Tech',
        designation: 'Manager',
        salary: 22200,
      },
    ],
  }
  count = (str) => {
    const { employees } = this.state
    return employees.reduce(
      (acc, curr) => (curr.dept === str ? acc + 1 : acc),
      0,
    )
  }
  totalSalary = () => {
    const { employees } = this.state
    return employees.reduce((acc, curr) => acc + curr.salary, 0)
  }
  avgSalary = () => {
    const { employees } = this.state
    return employees.reduce((acc, curr) => acc + curr.salary, 0)
  }
  maxSalary = () => {
    const { employees } = this.state
    return employees.reduce(
      (acc, curr) => (curr.salary > acc ? curr.salary : acc),
      0,
    )
  }
  minSalary = () => {
    const { employees } = this.state
    let a = employees.map((opt) => opt.salary)
    return a.reduce((acc, curr) => (curr < acc ? curr : acc))
  }
  employeeTable = () => {
    const { employees } = this.state
    return (
      <div className="container">
        <h1 className="text-center">Welcome to the Employee Protal</h1>
        <div className="row bg-light">
          {employees.map((st) => {
            const { name, level, dept, designation, salary } = st
            return (
              <div className="col-6 border text-center">
                <h4>{name}</h4>
                Department:{dept}
                <br />
                Designation:{designation}
              </div>
            )
          })}
        </div>
        <div className="row bg-warning">
          <div className="col-6 text-center">
            <h4>Employees Details</h4>
            Number of employees:{employees.length}
            <br />
            Tech:{this.count('Tech')}
            <br />
            Fin:{this.count('Fin')}
            <br />
            Ops:{this.count('Ops')}
            <br />
          </div>
          <div className="col-6 text-center">
            <h4>Salary Details</h4>
            Total Salary:{this.totalSalary()}
            <br />
            Average Salary:{this.avgSalary() / employees.length}
            <br />
            Max Salary:{this.maxSalary()}
            <br />
            Min Salary:{this.minSalary()}
            <br />
          </div>
        </div>
      </div>
    )
  }
  render() {
    return this.employeeTable()
  }
}
export default Name
