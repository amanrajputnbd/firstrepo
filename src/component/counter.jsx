import React, { Component } from 'react'
class Counter extends Component {
  render() {
    console.log('Props', this.props)
    const { counter, onDelete, onIncrement } = this.props
    const { alphabet, count, id } = counter
    return (
      <h5>
        counter{alphabet} count={count}
        <button
          className="btn btn-primary btn-sm m-2"
          onClick={() => onIncrement(id)}
        >
          Increment
        </button>
        <button
          className="btn btn-danger btn-sm m-2"
          onClick={() => onDelete(id)}
        >
          Delete
        </button>
      </h5>
    )
  }
}
export default Counter
