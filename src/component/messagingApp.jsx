import React, { Component } from 'react'
import MessagingFolder from './messagingFolder'
import Message from './message'
class MessagingApp extends Component {
  state = {
    msg: [
      {
        id: 121,
        sent: false,
        from: 'tweets@twitter.com',
        to: 'jack@test.com',
        subject: '18 tweets from those you follow',
        text:
          'Go to your twitter page and see the tweets from those you follow.',
        folder: 'Social',
        read: true,
      },
      {
        id: 141,
        sent: true,
        from: 'jack@test.com',
        to: 'mary@test.com',
        subject: 'Bug 461 in Customer Flow',
        text:
          'When the checkbox is left unchecked and the option Important is selected in the dropdown, clicking on Submit, shows no results.',
        folder: 'Sent',
        read: false,
      },
      {
        id: 158,
        sent: false,
        from: 'email@facebook.com',
        to: 'jack@test.com',
        subject: 'New post from William Jones',
        text:
          'William Jones has just uploaded a new post -How i loved the Avengers Endgame.',
        folder: 'Social',
        read: true,
      },
      {
        id: 177,
        sent: true,
        from: 'jack@test.com',
        to: 'williams@test.com',
        subject: 'Movie tomorrow',
        text: 'Avengers Endgame is releasing tomorrow. Wanna see.',
        folder: 'Sent',
        read: false,
      },
      {
        id: 179,
        sent: false,
        from: 'williams@test.com',
        to: 'jack@test.com',
        subject: 'Re: Movie tomorrow',
        text:
          'The movie is supposed to be a blast. Lets do the 8:30 show. Want to havea quick bite before.',
        folder: 'Inbox',
        read: false,
      },
      {
        id: 194,
        sent: false,
        from: 'retweet@twitter.com',
        to: 'jack@test.com',
        subject: 'Your tweet has been retweeted by Thomas',
        text:
          'Your tweet on the MarvelSuperheroes and Avengers has been retweeted bt Thomas. It has now 41 retweets and 27likes.',
        folder: 'Social',
        read: false,
      },
      {
        id: 204,
        sent: true,
        from: 'mary@test.com',
        to: 'jack@test.com',
        subject: 'To do on Friday',
        text:
          'Test the bugs on the employee form in Release 0.7.9 and fix them.',
        folder: 'Inbox',
        read: false,
      },
      {
        id: 255,
        sent: true,
        from: 'mary@test.com',
        to: 'jack@test.com',
        subject: 'Release 0.8.4 deployed',
        text: 'Release 0.8.4 has been deployed in the test environment.',
        folder: 'Inbox',
        read: false,
      },
      {
        id: 278,
        sent: false,
        from: 'mary@test.com',
        to: 'jack@test.com',
        subject: 'Re: Bug 461 in Customer Flow',
        text:
          'The bug has been fixed in the release 0.8.7. \nPlease test the issue and close it.\nCan you do it but tomorrow\nMary',
        folder: 'Inbox',
        read: false,
      },
      {
        id: 281,
        sent: true,
        from: 'jack@test.com',
        to: 'mary@test.com',
        subject: 'Re: Re: Bug 461 in Customer Flow',
        text: 'Bug 461 has been closed.\nRegards,\nJack',
        folder: 'Sent',
        read: false,
      },
      {
        id: 289,
        sent: false,
        from: 'email@facebook.com',
        to: 'jack@test.com',
        subject: '5 Shares, 2 Posts from your friends',
        text:
          "Jack, while you were away, your friends are having fun on Facebook.\nDon't miss their posts.\nKeep up with yourfriends.",
        folder: 'Social',
        read: true,
      },
    ],
    msg2: [],
    m: -1,
    message: [],
  }

  allMsg = (n) => {
    let s1 = { ...this.state }
    console.log(s1)
    if (n === 1) {
      s1.msg2 = s1.msg
    } else if (n === 2) {
      s1.msg2 = s1.msg.filter((m1) => m1.folder === 'Inbox')
    } else if (n === 3) {
      s1.msg2 = s1.msg.filter((m1) => m1.folder === 'Social')
    } else if (n === 4) {
      s1.msg2 = s1.msg.filter((m1) => m1.folder === 'Sent')
    } else {
      s1.msg2 = s1.msg
    }
    this.setState(s1)
  }

  showMsgData = (id) => {
    let s1 = { ...this.state }
    let msg = s1.msg2.find((m1) => m1.id === id)
    if (msg) {
      s1.m = 1
      msg.read = true
      s1.message.push(msg)
    }
    this.setState(s1)
  }

  back = () => {
    let s1 = { ...this.state }
    s1.m = -1
    s1.message = []
    this.setState(s1)
  }

  delete = (id) => {
    let s1 = { ...this.state }
    let index = s1.msg.findIndex((m1) => m1.id === id)
    s1.msg.splice(index, 1)
    let index1 = s1.msg2.findIndex((m1) => m1.id === id)
    s1.msg2.splice(index1, 1)
    s1.m = -1
    s1.message = []
    this.setState(s1)
  }

  render() {
    const { msg2, m, message } = this.state
    return (
      <div className="container">
        <div className="row">
          <div className="col-3 bg-dark text-white">
            <h3>Folder</h3>
            <a onClick={() => this.allMsg(1)}>All</a>
            <br />
            <span onClick={() => this.allMsg(2)}>Inbox</span>
            <br />
            <span onClick={() => this.allMsg(3)}>Social</span>
            <br />
            <span onClick={() => this.allMsg(4)}>Sent</span>
          </div>
          <div className="col-9 bg-light">
            {m < 0 ? (
              <React.Fragment>
                {msg2.map((m1) => (
                  <MessagingFolder
                    msg={m1}
                    showMsg={this.showMsgData}
                    show={this.show}
                  />
                ))}
              </React.Fragment>
            ) : (
              <React.Fragment>
                {message.map((m1) => (
                  <Message
                    message1={m1}
                    onBack={this.back}
                    onRemove={this.delete}
                  />
                ))}
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    )
  }
}
export default MessagingApp
