import { Button } from 'bootstrap'
import React, { Component } from 'react'
class Event extends Component {
  state = { numbers: [1, 3, 5, 7], expression: '', calcValue: 0, opt: '' }

  addA = (n) => {
    let s1 = { ...this.state }
    if (s1.opt === '+') {
      s1.calcValue += n
      s1.expression += n + '+'
      this.setState(s1)
    }
    if (s1.opt === '*') {
      s1.calcValue *= n
      s1.expression += n + '*'
      this.setState(s1)
    }
  }
  addOpretion = (str) => {
    let s1 = { ...this.state }
    s1.expression = ''
    str === '*' ? (s1.calcValue = 1) : (s1.calcValue = 0)
    s1.opt = str
    this.setState(s1)
  }
  render() {
    const { numbers, expression, calcValue, opt } = this.state
    return (
      <div className="container">
        Operator:
        <button
          className="btn btn-primary m-2"
          onClick={() => this.addOpretion('+')}
        >
          Add
        </button>
        <button
          className="btn btn-primary m-2"
          onClick={() => this.addOpretion('*')}
        >
          Multiply
        </button>
        <br />
        {numbers.map((st) => (
          <button className="btn btn-primary m-2" onClick={() => this.addA(st)}>
            {st}
          </button>
        ))}
        <br />
        Opertions={opt}
        <br />
        Expression={expression}
        <br />
        CalcValue={calcValue}
      </div>
    )
  }
}
export default Event
