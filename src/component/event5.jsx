import React, { Component } from 'react'
class Event extends Component {
  state = {
    text: '',
  }

  clickBtn = (str) => {
    let s1 = { ...this.state }
    s1.text += str
    this.setState(s1)
  }

  render() {
    return (
      <React.Fragment>
        <button
          className="btn btn-primary m-1"
          onClick={() => this.clickBtn('A')}
        >
          A
        </button>
        <button
          className="btn btn-primary m-1"
          onClick={() => this.clickBtn('B')}
        >
          B
        </button>
        <button
          className="btn btn-primary m-1"
          onClick={() => this.clickBtn('C')}
        >
          C
        </button>
        <button
          className="btn btn-primary m-1"
          onClick={() => this.clickBtn('D')}
        >
          D
        </button>
        <br />
        txt:{this.state.text}
      </React.Fragment>
    )
  }
}
export default Event
