import React, { Component } from 'react'
class MessagingFolder extends Component {
  render() {
    const { msg, showMsg } = this.props
    const { id, sent, from, to, subject, folder, read, text } = msg
    return (
      <div className="row border" onClick={() => showMsg(id)}>
        <div className="col-2">
          {read ? (
            <i class="bi bi-envelope-open"></i>
          ) : (
            <i class="bi bi-envelope"></i>
          )}
        </div>
        <div className="col-5">{read ? from : <b>{from}</b>}</div>
        <div className="col-5">{read ? subject : <b>{subject}</b>}</div>
      </div>
    )
  }
}
export default MessagingFolder
