import React, { Component } from 'react'
class Event extends Component {
  state = {
    products: [
      { name: 'Pepsi', price: 20 },
      { name: 'Dairy Milk', price: 40 },
      { name: 'Maggi', price: 15 },
      { name: 'Snickers', price: 50 },
      { name: 'Nescafe', price: 100 },
    ],
    cart: [],
  }
  addToCart = (index) => {
    let s1 = { ...this.state }
    let pr = s1.products[index]
    let x1 = s1.cart.find((st) => st.name === pr.name)
    x1 ? (x1.qty = x1.qty + 1) : s1.cart.push({ ...pr, qty: 1 })
    this.setState(s1)
  }
  render() {
    const { products, cart } = this.state
    return (
      <div className="container">
        {cart.length === 0 ? <h3>Cart is Empty</h3> : <h3>Cart</h3>}
        <ul>
          {cart.map((st) => {
            const { name, price, qty } = st
            return (
              <li>
                {name},Price={price},qty={qty}
              </li>
            )
          })}
        </ul>
        <div className="row border bg-dark text-white">
          <div className="col-4">Name</div>
          <div className="col-4 text-center">Price</div>
          <div className="col-4 text-center"></div>
        </div>
        {products.map((st, index) => {
          const { name, price } = st
          return (
            <div className="row border">
              <div className="col-4">{name}</div>
              <div className="col-4 text-center">{price}</div>
              <div className="col-4 text-center">
                <button
                  className="btn btn-secondary btn-sm"
                  onClick={() => this.addToCart(index)}
                >
                  Add
                </button>
              </div>
            </div>
          )
        })}
      </div>
    )
  }
}
export default Event
