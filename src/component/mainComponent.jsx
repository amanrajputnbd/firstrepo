import React, { Component } from 'react'
import NavBar from './navBar'
class Mainomponent extends Component {
  render() {
    let count = 10
    let qty = 20
    return (
      <React.Fragment>
        <NavBar count={count} qty={qty} />
        <div className="container">
          <h6>count={count}</h6>
          <h6>Quantity={qty}</h6>
        </div>
      </React.Fragment>
    )
  }
}
export default Mainomponent
