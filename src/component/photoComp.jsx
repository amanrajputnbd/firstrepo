import React, { Component } from 'react'
class PhotoComp extends Component {
  brColor = (para) =>
    para === 'like'
      ? 'bg-success'
      : para === 'dislike'
      ? 'bg-warning'
      : 'bg-light'

  render() {
    const { photo, index, onLike, onDislike, onDelete } = this.props
    const { img, like, dislike, myOption } = photo
    return (
      <div className={'col-4 border p-2  ' + this.brColor(myOption)}>
        <img src={img} alt="" />
        <br />
        <button
          className="btn btn-primary btn-sm m-1"
          onClick={() => onLike(index)}
        >
          <i
            className={
              myOption === 'like' ? 'fas fa-thumbs-up' : 'far fa-thumbs-up'
            }
          >
            {like}
          </i>
        </button>
        <button
          className="btn btn-primary btn-sm m-1"
          onClick={() => onDislike(index)}
        >
          <i
            className={
              myOption === 'dislike'
                ? 'fas fa-thumbs-down'
                : 'far fa-thumbs-down'
            }
          >
            {dislike}
          </i>
        </button>
        <button
          className="btn btn-primary btn-sm m-1"
          onClick={() => onDelete(index)}
        >
          <i className="fa fa-trash"></i>
        </button>
      </div>
    )
  }
}
export default PhotoComp
