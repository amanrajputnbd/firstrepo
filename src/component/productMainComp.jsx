import React, { Component } from 'react'
import LeftPannel from './leftpannelOfProducts'
class ProductMainComp extends Component {
  state = {
    products: [
      { id: 'PEP110', name: 'Pepsi', category: 'Food', stock: 'yes' },
      { id: 'CLO876', name: 'Close Up', category: 'Toothpaste', stock: 'no' },
      { id: 'PEA531', name: 'Pears', category: 'Soap', stock: 'arriving' },
      { id: 'LU7264', name: 'Lux', category: 'Soap', stock: 'yes' },
      { id: 'COL112', name: 'Colgate', category: 'Toothpaste', stock: 'no' },
      { id: 'DM881', name: 'Dairy Milk', category: 'Food', stock: 'arriving' },
      { id: 'LI130', name: 'Liril', category: 'Soap', stock: 'yes' },
      { id: 'PPS613', name: 'Pepsodent', category: 'Toothpaste', stock: 'no' },
      { id: 'MAG441', name: 'Maggi', category: 'Food', stock: 'arriving' },
      { id: 'PNT560', name: 'Pantene', category: 'Shampoo', stock: 'no' },
      { id: 'KK219', name: 'KitKat', category: 'Food', stock: 'arriving' },
      { id: 'DOV044', name: 'Dove', category: 'Soap', stock: 'yes' },
    ],

    optionsSel: {
      category: [],
      stock: '',
    },
  }
  showProduct = () => {
    const { products, optionsSel } = this.state
    const { category, stock } = optionsSel
    const product1 =
      category.length > 0
        ? products.filter(
            (lt) => category.findIndex((br) => br === lt.category) >= 0,
          )
        : products

    const product2 = stock
      ? product1.filter((lt) => lt.stock === stock)
      : product1.length < 0
      ? []
      : product1

    return (
      <div className="container">
        <b>Selected Products</b>
        {product2.map((lt) => (
          <div className="row bg-light">
            <div className="col">{lt.name}</div>
            <div className="col">{lt.category}</div>
            <div className="col">{lt.id}</div>
            <div className="col">{lt.stock}</div>
          </div>
        ))}
      </div>
    )
  }
  handleChangeOption = (optionsSel) => {
    let s1 = { ...this.state }
    s1.optionsSel = optionsSel
    this.setState(s1)
  }
  handleClear = () => {
    let s1 = { ...this.state }
    s1.optionsSel = {
      brand: [],
      ram: [],
      processor: '',
      rating: '',
    }
    this.setState(s1)
  }

  render() {
    const { optionsSel } = this.state
    let catArr = this.state.products.map((p1) => p1.category)
    catArr = [...new Set(catArr)]
    let stockArr = this.state.products.map((p1) => p1.stock)
    stockArr = [...new Set(stockArr)]
    const optionsArray = { category: catArr, stock: stockArr }
    return (
      <div className="container">
        <div className="row">
          <div className="col-3 border bg-light">
            <LeftPannel
              optionsSel={optionsSel}
              optionsArray={optionsArray}
              onChangeOption={this.handleChangeOption}
              onClear={this.handleClear}
            />
          </div>
          <div className="col-9 border">{this.showProduct()}</div>
        </div>
      </div>
    )
  }
}
export default ProductMainComp
