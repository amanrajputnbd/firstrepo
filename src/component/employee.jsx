import React, { Component, useLayoutEffect } from 'react'
class Name extends Component {
  state = {
    employees: [
      {
        name: 'Jack Smith',
        level: 2,
        dept: 'Tech',
        designation: 'Manager',
        salary: 24000,
      },
      {
        name: 'Mary Robbins',
        level: 3,
        dept: 'Fin',
        designation: 'Manager',
        salary: 28000,
      },
      {
        name: 'Steve Williams',
        level: 4,
        dept: 'Ops',
        designation: 'President',
        salary: 35000,
      },
      {
        name: 'Bob Andrews',
        level: 1,
        dept: 'Fin',
        designation: 'Trainee',
        salary: 16500,
      },
      {
        name: 'Dave Martin',
        level: 2,
        dept: 'Fin',
        designation: 'Manager',
        salary: 21700,
      },
      {
        name: 'Julia Clarke',
        level: 3,
        dept: 'Ops',
        designation: 'Manager',
        salary: 26900,
      },
      {
        name: 'Kathy Jones',
        level: 4,
        dept: 'Tech',
        designation: 'President',
        salary: 42500,
      },
      {
        name: 'Tom Bresnan',
        level: 2,
        dept: 'Tech',
        designation: 'Manager',
        salary: 22200,
      },
    ],
  }
  //sortSalary = (n1, n2) => n1.name.localeCompare(n2.name)
  sortLevel = (n1, n2) => n2.level - n1.level
  employeeTable = () => {
    const { employees } = this.state
    const employees1 = [...employees].sort(this.sortLevel)
    //const employees1 = [...employees].filter((opt) => opt.level > 1)
    return (
      <div className="container">
        <div className="row border bg-dark text-white">
          <div className="col-4 border">Name</div>
          <div className="col-2 border text-center">Level</div>
          <div className="col-2 border text-center">Department</div>
          <div className="col-2 border text-center">Designation</div>
          <div className="col-2 border text-center">Salary</div>
        </div>
        {employees1.map((st) => {
          const { name, level, dept, designation, salary } = st
          return (
            <div className="row border">
              <div className="col-4 border">{name}</div>
              <div className="col-2 border text-center">{level}</div>
              <div className="col-2 border text-center">{dept}</div>
              <div className="col-2 border text-center">{designation}</div>
              <div className="col-2 border text-center">{salary}</div>
            </div>
          )
        })}
      </div>
    )
  }
  render() {
    return this.employeeTable()
  }
}
export default Name
