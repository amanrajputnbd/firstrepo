import React, { Component } from 'react'
class Event extends Component {
  state = {
    persons: [
      {
        name: 'Jack',
        age: 25,
        skills: 'Javascript,React',
        email: 'jack12@email.com',
        mobile: '541226548',
      },
      {
        name: 'Anna',
        age: 29,
        skills: 'Javascript,Python',
        email: 'anna32@email.com',
        mobile: '547226548',
      },
      {
        name: 'Steve',
        age: 31,
        skills: 'Python,React',
        email: 'steve312@email.com',
        mobile: '541221520',
      },
    ],
    indexPerson: -1,
    showContact: false,
  }

  setIndex = (index) => {
    let s1 = { ...this.state }
    s1.indexPerson = index
    s1.showContact = false
    this.setState(s1)
  }
  showPerson = () => {
    const { persons, indexPerson, showContact } = this.state
    return (
      <React.Fragment>
        Name:{persons[indexPerson].name}
        <br />
        Age:{persons[indexPerson].age}
        <br />
        Skills:{persons[indexPerson].skills}
        <br />
        <button className="btn btn-primary" onClick={() => this.showContact()}>
          Contact Info
        </button>
        <br />
        {showContact ? (
          <React.Fragment>
            Email:{persons[indexPerson].email}
            <br />
            Mobile:{persons[indexPerson].mobile}
            <br />
          </React.Fragment>
        ) : (
          ''
        )}
      </React.Fragment>
    )
  }

  showContact = () => {
    let s1 = { ...this.state }
    s1.showContact = true
    this.setState(s1)
  }
  render() {
    const { persons, indexPerson } = this.state
    return (
      <React.Fragment>
        {persons.map((opt, index) => (
          <button
            className="btn btn-primary m-3"
            onClick={() => this.setIndex(index)}
          >
            {opt.name}
          </button>
        ))}
        <br />
        {indexPerson >= 0 ? this.showPerson() : ''}
      </React.Fragment>
    )
  }
}
export default Event
