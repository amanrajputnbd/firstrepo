import { Button } from 'bootstrap'
import React, { Component } from 'react'
class Event extends Component {
  state = {
    priceData: [
      { name: 'Perk', category: 'Food', oldPrice: 10, newPrice: 10 },
      { name: '5Star', category: 'Food', oldPrice: 15, newPrice: 12 },
      { name: 'Pepsi', category: 'Drink', oldPrice: 20, newPrice: 22 },
      { name: 'Maggi', category: 'Food', oldPrice: 12, newPrice: 15 },
      { name: 'Coke', category: 'Drink', oldPrice: 20, newPrice: 20 },
      { name: 'Gems', category: 'Food', oldPrice: 10, newPrice: 10 },
      { name: '7Up', category: 'Drink', oldPrice: 15, newPrice: 17 },
      { name: 'Lindt', category: 'Food', oldPrice: 80, newPrice: 90 },
      { name: 'Nutties', category: 'Food', oldPrice: 20, newPrice: 18 },
      { name: 'Slice', category: 'Drink', oldPrice: 18, newPrice: 16 },
    ],
    txt: '',
  }
  sort = (col, str) => {
    let s1 = { ...this.state }
    switch (col) {
      case 0:
        s1.priceData.sort((p1, p2) => p1.name.localeCompare(p2.name))
        s1.txt = 'Sorted By ' + str
        break
      case 1:
        s1.priceData.sort((p1, p2) => p1.category.localeCompare(p2.category))
        s1.txt = 'Sorted By ' + str
        break
      case 2:
        s1.priceData.sort((p1, p2) => +p1.oldPrice - +p2.oldPrice)
        s1.txt = 'Sorted By ' + str
        break
      case 3:
        s1.priceData.sort((p1, p2) => +p1.newPrice - +p2.newPrice)
        s1.txt = 'Sorted By ' + str
        break
    }
    this.setState(s1)
  }
  filterTable = (str) => {
    let s1 = { ...this.state }
    if (str === 'Food') {
      s1.priceData = s1.priceData.filter((st) => st.category === str)
      s1.txt = 'Filter: By Food'
    } else if (str === 'Drink') {
      s1.priceData = s1.priceData.filter((st) => st.category === str)
      s1.txt = 'Filter: By Drink'
    } else if (str === 'Increase') {
      s1.priceData = s1.priceData.filter((st) => st.newPrice > st.oldPrice)
      s1.txt = 'Filter:Increase by Price'
    } else if (str === 'Decrease') {
      s1.priceData = s1.priceData.filter((st) => st.newPrice < st.oldPrice)
      s1.txt = 'Filter:Decrease by Price'
    } else if (str === 'Same') {
      s1.priceData = s1.priceData.filter((st) => st.newPrice === st.oldPrice)
      s1.txt = 'Filter:Same by Price'
    }
    this.setState(s1)
  }
  render() {
    const { priceData, txt } = this.state
    return (
      <div className="container">
        {txt === '' ? <h2>Not Sorted</h2> : <h2>{txt}</h2>}
        <button
          className="btn btn-primary m-3"
          onClick={() => this.filterTable('Food')}
        >
          Food
        </button>
        <button
          className="btn btn-primary m-3"
          onClick={() => this.filterTable('Drink')}
        >
          Drink
        </button>
        <button
          className="btn btn-primary m-3"
          onClick={() => this.filterTable('Increase')}
        >
          Increase
        </button>
        <button
          className="btn btn-primary m-3"
          onClick={() => this.filterTable('Decrease')}
        >
          Decrease
        </button>
        <button
          className="btn btn-primary m-3"
          onClick={() => this.filterTable('Same')}
        >
          Same
        </button>

        <div className="row border bg-dark text-white">
          <div className="col-4" onClick={() => this.sort(0, 'Name')}>
            Name
          </div>
          <div
            className="col-2 text-center"
            onClick={() => this.sort(1, 'Category')}
          >
            category
          </div>
          <div
            className="col-2 text-center"
            onClick={() => this.sort(2, 'Old Price')}
          >
            Old Price
          </div>
          <div
            className="col-2 text-center"
            onClick={() => this.sort(3, 'New Price')}
          >
            New Price
          </div>
        </div>
        {priceData.map((st) => {
          const { name, category, oldPrice, newPrice } = st
          return (
            <div className="row border">
              <div className="col-4">{name}</div>
              <div className="col-2 text-center">{category}</div>
              <div className="col-2 text-center">{oldPrice}</div>
              <div className="col-2 text-center">{newPrice}</div>
            </div>
          )
        })}
      </div>
    )
  }
}
export default Event
