import React, { Component } from 'react'
class PicViewer extends Component {
  render() {
    const { pics, index, currentIndex, addFav } = this.props
    return (
      <React.Fragment>
        <div className="col-12">
          {index === currentIndex ? <img src={pics} alt="" /> : ''}
        </div>
        <div className="col mt-2">
          {index === currentIndex ? (
            <button className="btn btn-primary" onClick={() => addFav(index)}>
              Add to Favorites
            </button>
          ) : (
            ''
          )}
        </div>
      </React.Fragment>
    )
  }
}
export default PicViewer
