import React, { Component } from 'react'
class SimpleForm extends Component {
  state = {
    person: this.props.person,
  }

  handleChange = (e) => {
    //console.log(e.currentTarget)
    let s1 = { ...this.state }
    //if (e.currentTarget.name === 'name') s1.person.name = e.currentTarget.value
    //if (e.currentTarget.name === 'age') s1.person.age = e.currentTarget.value
    //if (e.currentTarget.name === 'name') s1.person['name'] = e.currentTarget.value
    //if (e.currentTarget.name === 'age') s1.person['age'] = e.currentTarget.value
    s1.person[e.currentTarget.name] = e.currentTarget.value
    this.setState(s1)
  }

  handleSubmit = (e) => {
    e.preventDefault()
    console.log('Handle Submit', this.state.person)
    this.props.onSubmit(this.state.person)
  }

  render() {
    let { coursename, lectures, faculty } = this.state.person
    return (
      <div className="container">
        <h5>{this.props.edit ? 'Edit Details' : 'Enter Details of Person'}</h5>
        <div className="form-group">
          <label htmlFor="">Course Name</label>
          <input
            type="text"
            className="form-control"
            id="coursename"
            name="coursename"
            value={coursename}
            placeholder="Enter the name"
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="">Faculty</label>
          <input
            type="text"
            className="form-control"
            id="faculty"
            name="faculty"
            value={faculty}
            placeholder="Enter the name"
            onChange={this.handleChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="">Lectures</label>
          <input
            type="text"
            className="form-control"
            id="lectures"
            name="lectures"
            value={lectures}
            placeholder="Enter the name"
            onChange={this.handleChange}
          />
        </div>
        <button className="btn btn-primary" onClick={this.handleSubmit}>
          {this.props.edit ? 'Update' : 'Submit'}
        </button>
      </div>
    )
  }
}
export default SimpleForm
