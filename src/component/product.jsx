import React, { Component } from 'react'
class Product extends Component {
  render() {
    let { products, onDecrease, onIncrease } = this.props
    let { name, code, price, qty } = products
    return (
      <div className="col-2 bg-info px-6 m-4">
        {name}
        <br />
        Code:{code}
        <br />
        Price:{price}
        <br />
        Quantity:{qty}
        <br />
        <button
          className="btn btn-light btn-sm "
          onClick={() => onIncrease(code)}
        >
          Increase
        </button>
        {qty === 0 ? (
          <button className="btn btn-light btn-sm m-1 " disabled>
            Decrease
          </button>
        ) : (
          <button
            className="btn btn-light btn-sm m-1"
            onClick={() => onDecrease(code)}
          >
            Decrease
          </button>
        )}
      </div>
    )
  }
}
export default Product
