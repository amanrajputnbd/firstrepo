import React, { Component } from 'react'
class LeftPannel extends Component {
  handleChange = (e) => {
    let s1 = { ...this.props.optionsSel }
    let { currentTarget: input } = e
    input.name === 'processor'
      ? (s1.processor = this.updateCBs(
          input.checked,
          input.value,
          s1.processor,
        ))
      : input.name === 'rating'
      ? (s1.rating = this.updateCBs(input.checked, input.value, s1.rating))
      : input.name === 'hardDisk'
      ? (s1.hardDisk = this.updateCBs(input.checked, input.value, s1.hardDisk))
      : (s1[input.name] = input.value)
    this.props.onChangeOption(s1)
  }

  updateCBs = (checked, value, arr) => {
    if (checked) arr.push(value)
    else {
      let index = arr.findIndex((ele) => ele === value)
      if (index >= 0) arr.splice(index, 1)
    }
    return arr
  }

  render() {
    const { optionsSel, optionsArray } = this.props
    return (
      <div className="container">
        <h6>Choose Options</h6>
        <button className="btn btn-warning btn-sm" onClick={this.props.ocClear}>
          All Clear
        </button>
        <br />
        {this.showDropdown(
          'Brand',
          optionsArray.brand,
          'brand',
          'select the Brand',
          optionsSel.brand,
        )}
        {this.showDropdown(
          'Ram',
          optionsArray.ram,
          'ram',
          'Select the Ram',
          optionsSel.ram,
        )}
        {this.showCheckboxes(
          'Processor',
          optionsArray.processor,
          'processor',
          optionsSel.processor,
        )}
        {this.showCheckboxes(
          'Ratings',
          optionsArray.rating,
          'rating',
          optionsSel.rating,
        )}
        {this.showCheckboxes(
          'Hard Disk',
          optionsArray.hardDisk,
          'hardDisk',
          optionsSel.hardDisk,
        )}
      </div>
    )
  }

  showCheckboxes = (lable, arr, name, selArr) => {
    return (
      <React.Fragment>
        <label className="form-check-lable">
          <b>{lable}</b>
        </label>
        {arr.map((opt, index) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name={name}
              value={opt}
              checked={selArr.findIndex((sel) => sel === opt) >= 0}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{opt}</label>
          </div>
        ))}
      </React.Fragment>
    )
  }

  showDropdown = (lable, arr, name, header, value) => {
    return (
      <div className="form-group">
        <label>
          <b>{lable}</b>
        </label>
        <select
          name={name}
          className="form-control"
          value={value}
          onChange={this.handleChange}
        >
          <option value="" disabled>
            {header}
          </option>
          {arr.map((c1) => (
            <option>{c1}</option>
          ))}
        </select>
      </div>
    )
  }

  showRadios = (lable, arr, name, selVal) => {
    return (
      <React.Fragment>
        <label className="form-check-lable font-weight-bold">
          <b>{lable}</b>
        </label>
        {arr.map((opt) => (
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name={name}
              value={opt}
              checked={selVal === opt}
              onChange={this.handleChange}
            />
            <label className="form-check-lable">{opt}</label>
          </div>
        ))}
      </React.Fragment>
    )
  }
}
export default LeftPannel
