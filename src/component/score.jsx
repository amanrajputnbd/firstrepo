import React, { Component } from 'react'
class Score extends Component {
  state = {
    players: [
      { name: 'Williams', answered: 10, correct: 8 },
      { name: 'George', answered: 8, correct: 7 },
      { name: 'Katherine', answered: 12, correct: 9 },
      { name: 'Sophia', answered: 11, correct: 10 },
      { name: 'Timothy', answered: 9, correct: 6 },
      { name: 'Jack', answered: 13, correct: 8 },
    ],
    correctScore: 2,
    incorrectScore: -1,
  }
  sortPlayers = (p1, p2) => {
    const { correctScore, incorrectScore } = this.state
    let x1 =
      p1.correct * correctScore + (p1.answered - p1.correct) * incorrectScore
    let x2 =
      p2.correct * correctScore + (p2.answered - p2.correct) * incorrectScore
    return x2 - x1
  }
  totalQns = () => {
    const { players } = this.state
    return players.reduce((acc, curr) => acc + curr.answered, 0)
  }
  totalCorrect = () => {
    const { players } = this.state
    return players.reduce((acc, curr) => acc + curr.correct, 0)
  }
  render() {
    const { players, correctScore, incorrectScore } = this.state
    const players1 = [...players]
    let totalQnsAsk = this.totalQns()
    let totalCorrectQns = this.totalCorrect()
    players1.sort(this.sortPlayers)
    return (
      <div className="container">
        <div className="row ">
          {players.map((st) => {
            let { name, answered, correct } = st
            return (
              <div className="col-4 border bg-warning">
                {name}
                <br />
                Answered:{answered}
                <br />
                Correct:{correct}
                <br />
                Score:
                {correct * correctScore + (answered - correct) * incorrectScore}
              </div>
            )
          })}
        </div>
        <div className="row bg-light">
          <div className="col-6 border">
            <h4>Leaderboard</h4>
            1.{players1[0].name}
            <br />
            2.{players1[1].name}
            <br />
            3.{players1[2].name}
            <br />
          </div>
          <div className="col-6">
            <h4>Statistics</h4>
            Total Questions: {totalQnsAsk}
            <br />
            Correct Answers:{totalCorrectQns} <br />
            Incorrect Answers:{totalQnsAsk - totalCorrectQns} <br />
          </div>
        </div>
      </div>
    )
  }
}
export default Score
