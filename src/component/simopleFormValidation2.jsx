import React, { Component } from 'react'
class SimpleForm extends Component {
  state = {
    persons: {
      name: '',
      description: '',
      productcode: '',
      category: '',
      price: '',
      discount: '',
    },
    errors: {},
    discounts: ['5%', '10%', '20%'],
    categorys: ['Food', 'Electronics', 'Apparels', 'Grocery'],
  }

  handleChange = (e) => {
    let { currentTarget: input } = e
    let s1 = { ...this.state }
    s1.persons[input.name] = input.value
    this.handleValidate(e)
    this.setState(s1)
  }

  isValid = (errors) => {
    //errors would have keys with non empty string as vslue
    let keys = Object.keys(errors)
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0)
    return count === 0
  }

  handleSubmit = (e) => {
    //e.preventDefault()
    let errors = this.validateAll()
    if (this.isValid(errors)) alert('Successfully Submitted')
    else {
      let s1 = { ...this.state }
      s1.errors = errors
      this.setState(s1)
    }
  }

  handleValidate = (e) => {
    //only the field which the event has beeb fired
    let { currentTarget: input } = e
    let s1 = { ...this.state }
    switch (input.name) {
      case 'name':
        s1.errors.name = this.validateName(input.value)
        break
      case 'description':
        s1.errors.description = this.validateDescription(input.value)
        break
      case 'productcode':
        s1.errors.productcode = this.validateProductCode(input.value)
        break
      case 'category':
        s1.errors.categorya = this.validateCategory(input.value)
        break
      case 'price':
        s1.errors.price = this.validateCategory(input.value)
        break
      case 'discount':
        s1.errors.discount = this.validateCategory(input.value)
        break
      default:
        break
    }
    this.setState(s1)
  }

  isFormValid = () => {
    let errors = this.validateAll()
    return this.isValid(errors)
  }

  validateAll = () => {
    let {
      name,
      price,
      productcode,
      category,
      discount,
      description,
    } = this.state.persons
    let errors = {}
    errors.name = this.validateName(name)
    errors.description = this.validateDescription(description)
    errors.productcode = this.validateProductCode(productcode)
    errors.category = this.validateCategory(category)
    errors.price = this.validatePrice(price)
    errors.discount = this.validateDiscount(discount)
    return errors
  }

  validateName = (name) =>
    !name
      ? 'Name must be enter'
      : name.length < 5
      ? 'Name should have mininum 5 character'
      : /^[A-Za-z\s]+$/.test(name)
      ? ''
      : 'Enter the correct Name'

  validateDescription = (description) =>
    !description
      ? 'Enter the description'
      : description.length < 10
      ? 'description should have mininum 10 character'
      : description.match(/^[a-zA-Z\s]+$/)
      ? ''
      : 'Enter the description proper'

  validateProductCode = (productcode) =>
    !productcode
      ? 'Product code must be enter'
      : productcode.length < 0 && productcode.length > 6
      ? 'product code should of length 6'
      : /^[A-Z]{2}[0-9]{4}$/.test(productcode)
      ? ''
      : 'Enter the proper code'

  validateCategory = (category) => (!category ? 'select the category' : '')
  validatePrice = (price) =>
    !price ? 'Enter the price' : !+price ? 'Enter the correct price' : ''
  validateDiscount = (discount) =>
    !discount ? 'one of them should be checked' : ''

  render() {
    let {
      name,
      description,
      price,
      category,
      discount,
      productcode,
    } = this.state.persons
    let { errors, discounts, categorys } = this.state
    return (
      <div className="container">
        <div className="form-group">
          <label htmlFor="">Name</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={name}
            placeholder="Enter the name"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.name ? (
            <span className="text-danger">{errors.name}</span>
          ) : (
            ''
          )}
        </div>
        <div className="form-group">
          <label htmlFor="">Description</label>
          <input
            type="text"
            className="form-control"
            id="description"
            name="description"
            value={description}
            placeholder="Enter the Description of the product"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.description ? (
            <span className="text-danger">{errors.description}</span>
          ) : (
            ''
          )}
        </div>
        <div className="form-group">
          <label htmlFor="">Product Code</label>
          <input
            type="text"
            className="form-control"
            id="email"
            name="productcode"
            value={productcode}
            placeholder="Enter the Product Code"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.productcode ? (
            <span className="text-danger">{errors.productcode}</span>
          ) : (
            ''
          )}
        </div>
        <div className="form-group">
          <label>Category</label>
          <select
            name="category"
            className="form-control"
            value={category}
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          >
            <option value="" disabled>
              Select the Category
            </option>
            {categorys.map((c1) => (
              <option>{c1}</option>
            ))}
          </select>
          {errors.category ? (
            <span className="text-danger">{errors.category}</span>
          ) : (
            ''
          )}
        </div>
        <div className="form-group">
          <label htmlFor="">Price</label>
          <input
            type="text"
            className="form-control"
            id="price"
            name="price"
            value={price}
            placeholder="Enter the price"
            onChange={this.handleChange}
            onBlur={this.handleValidate}
          />
          {errors.price ? (
            <span className="text-danger">{errors.price}</span>
          ) : (
            ''
          )}
        </div>
        <div>
          <label className="form-check-lable font-weight-bold">
            <b>Choose the Discount</b>
          </label>
          {discounts.map((d1) => (
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="discount"
                value={d1}
                checked={discount === d1}
                onChange={this.handleChange}
                onBlur={this.handleValidate}
              />
              <label className="form-check-lable">{d1}</label>
            </div>
          ))}
          {errors.discount ? (
            <span className="text-danger">{errors.discount}</span>
          ) : (
            ''
          )}
        </div>
        <br />
        <button
          className="btn btn-primary"
          onClick={() => this.handleSubmit()}
          disabled={!this.isFormValid()}
        >
          Submit
        </button>
      </div>
    )
  }
}
export default SimpleForm
