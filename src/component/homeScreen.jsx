import React, { Component } from 'react'
import CustomerForm from './customerForm'
class HomeScreen extends Component {
  state = {
    view: -1,
    customers: [],
    customerEditIndex: -1,
  }

  showTable = () => {
    return (
      <React.Fragment>
        {this.state.customers.length === 0 ? (
          <h4>There are ZERO customer</h4>
        ) : (
          <React.Fragment>
            <div className="row bg-dark text-white">
              <div className="col-2">Name</div>
              <div className="col-2">Gender</div>
              <div className="col-2">Delivery</div>
              <div className="col-2">Payment</div>
              <div className="col-2">slot</div>
              <div className="col-2"></div>
            </div>
            {this.state.customers.map((c1, index) => {
              let { name, gender, deliver, payOpt, slot } = c1
              return (
                <div className="row bg-light">
                  <div className="col-2 border">{name}</div>
                  <div className="col-2 border">{gender}</div>
                  <div className="col-2 border">{deliver}</div>
                  <div className="col-2 border">{payOpt}</div>
                  <div className="col-2 border">{slot}</div>
                  <div className="col-2 border">
                    <button
                      className="btn btn-primary btn-sm"
                      onClick={() => this.handleEdit(index)}
                    >
                      Edit
                    </button>
                  </div>
                </div>
              )
            })}
          </React.Fragment>
        )}
      </React.Fragment>
    )
  }

  handleSubmit = (customer) => {
    let s1 = { ...this.state }
    s1.customerEditIndex >= 0
      ? (s1.customers[s1.customerEditIndex] = customer)
      : s1.customers.push(customer)
    s1.view = 2
    s1.customerEditIndex = -1
    this.setState(s1)
  }

  handleNewCust = () => {
    let s1 = { ...this.state }
    s1.view = 1
    this.setState(s1)
  }
  handleList = () => {
    let s1 = { ...this.state }
    s1.view = 2
    this.setState(s1)
  }
  handleEdit = (index) => {
    let s1 = { ...this.state }
    s1.customerEditIndex = index
    s1.view = 1
    this.setState(s1)
  }

  render() {
    let { view, customerEditIndex, customers } = this.state
    let coustomer = {
      name: '',
      gender: '',
      payOpt: [],
      deliver: '',
      slot: '',
    }
    return (
      <div className="container">
        <button
          className="btn btn-primary m-2"
          onClick={() => this.handleNewCust()}
        >
          New Customer
        </button>
        <button
          className="btn btn-primary m-2"
          onClick={() => this.handleList()}
        >
          List of Customers
        </button>
        {view === 2 ? (
          this.showTable()
        ) : view === 1 ? (
          <CustomerForm
            customer={
              customerEditIndex >= 0 ? customers[customerEditIndex] : coustomer
            }
            onSubmit={this.handleSubmit}
          />
        ) : (
          <h4>Welcome to the Customer System</h4>
        )}
      </div>
    )
  }
}
export default HomeScreen
