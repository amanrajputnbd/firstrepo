import React, { Component } from 'react'
class Favorites extends Component {
  render() {
    const { favorites, onRemove, index } = this.props
    return (
      <div className="col-1">
        <span onClick={() => onRemove(index)}>
          <img src={favorites} alt="" style={{ width: '40px' }} />
        </span>
      </div>
    )
  }
}
export default Favorites
