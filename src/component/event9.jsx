import { Button } from 'bootstrap'
import React, { Component } from 'react'
class Event extends Component {
  state = {
    products: [
      { name: 'Perk', price: 10, sales: 7 },
      { name: '5Star', price: 15, sales: 9 },
      { name: 'Pepsi', price: 20, sales: 20 },
      { name: 'Maggi', price: 12, sales: 15 },
      { name: 'Coke', price: 20, sales: 50 },
      { name: 'Lindt', price: 80, sales: 4 },
    ],
    txt: '',
  }
  sort = (col, str) => {
    let s1 = { ...this.state }
    let s = s1.products.map((st) => {
      let val = st.price * st.sales
      return { ...st, value: val }
    })
    s1.products = s
    switch (col) {
      case 0:
        s1.products.sort((p1, p2) => p1.name.localeCompare(p2.name))
        s1.txt = 'Sorted By' + str
        break
      case 1:
        s1.products.sort((p1, p2) => +p1.price - +p2.price)
        s1.txt = 'Sorted By' + str
        break
      case 2:
        s1.products.sort((p1, p2) => +p1.sales - +p2.sales)
        s1.txt = 'Sorted By' + str
        break
      case 3:
        s1.products.sort((p1, p2) => +p1.value - +p2.value)
        s1.txt = 'Sorted By' + str
        break
    }
    this.setState(s1)
  }
  filterValue = () => {
    console.log(this.state)
    let s1 = { ...this.state }
    s1.products = s1.products.map((st) => {
      let val = st.price * st.sales
      return { ...st, value: val }
    })
    s1.products = s1.products.filter((st) => st.value >= 100)
    s1.txt = 'Filter:Value>=100'
    this.setState(s1)
  }
  filterPrice = () => {
    let s1 = { ...this.state }
    console.log(this.state)
    s1.products = s1.products.filter((st) => st.price >= 15)
    s1.txt = 'Filter:Price>=15'
    this.setState(s1)
  }
  filterSales = () => {
    let s1 = { ...this.state }
    console.log(this.state)
    s1.products = s1.products.filter((st) => st.sales >= 10)
    s1.txt = 'Filter:Sales>=10'

    this.setState(s1)
  }
  render() {
    const { products, txt } = this.state
    return (
      <div className="container">
        {txt === '' ? <h2>Not Sorted</h2> : <h2> {txt}</h2>}
        <button
          className="btn btn-primary m-3"
          onClick={() => this.filterPrice()}
        >
          Price=15
        </button>
        <button
          className="btn btn-primary m-3"
          onClick={() => this.filterSales()}
        >
          Sales=10
        </button>
        <button
          className="btn btn-primary m-3"
          onClick={() => this.filterValue()}
        >
          Value=100
        </button>

        <div className="row border bg-dark text-white">
          <div className="col-4" onClick={() => this.sort(0, 'Name')}>
            Name
          </div>
          <div
            className="col-2 text-center"
            onClick={() => this.sort(1, 'Price')}
          >
            Price
          </div>
          <div
            className="col-2 text-center"
            onClick={() => this.sort(2, 'Sales')}
          >
            Sales
          </div>
          <div
            className="col-2 text-center"
            onClick={() => this.sort(3, 'Value')}
          >
            Value
          </div>
        </div>
        {products.map((st) => {
          const { name, price, sales } = st
          return (
            <div className="row border">
              <div className="col-4">{name}</div>
              <div className="col-2 text-center">{price}</div>
              <div className="col-2 text-center">{sales}</div>
              <div className="col-2 text-center">{sales * price}</div>
            </div>
          )
        })}
      </div>
    )
  }
}
export default Event
