import { Button } from 'bootstrap'
import React, { Component } from 'react'
class Event extends Component {
  state = {
    codeType: -1,
    arr1: ['A', 'B', 'C', 1, 2, 3],
    arr2: ['M', 'N', 0, 1, 2, 3],
    arr3: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    code: '',
    newArr: [],
  }
  create = (c) => {
    let s1 = { ...this.state }
    s1.code += c
    this.setState(s1)
  }
  add = () => {
    let s1 = { ...this.state }
    if (s1.code === '') return alert('Enter the code')
    let x = s1.newArr.find((st) => st === s1.code)
    if (x) alert('Code already Exist')
    else {
      s1.newArr.push(s1.code)
      s1.code = ''
    }
    this.setState(s1)
  }
  clearCode = () => {
    let s1 = { ...this.state }
    if (s1.code === '') {
      s1.code = ''
      s1.codeType = -1
    } else {
      s1.code = ''
    }
    this.setState(s1)
  }
  makeButton = (array) => {
    const { arr1, code, newArr, arr2, arr3, codeType } = this.state
    return (
      <React.Fragment>
        <br />
        Code so far:{code}
        <br />
        {array.map((st) => (
          <button
            className="btn-warning m-2 px-2"
            onClick={() => this.create(st)}
          >
            {st}
          </button>
        ))}
        <br />
        <button className="btn-primary m-3" onClick={() => this.add()}>
          Add New Code
        </button>
        <button className="btn-primary m-3" onClick={() => this.clearCode()}>
          Clear
        </button>
      </React.Fragment>
    )
  }
  codeABC = () => {
    let s1 = { ...this.state }
    s1.codeType = 1
    this.setState(s1)
  }
  code0_9 = () => {
    let s1 = { ...this.state }
    s1.codeType = 2
    this.setState(s1)
  }
  codeMN = () => {
    let s1 = { ...this.state }
    s1.codeType = 3
    this.setState(s1)
  }
  render() {
    const { arr1, code, newArr, arr2, arr3, codeType } = this.state
    return (
      <div className="container">
        <h1>Create New Code</h1>
        {codeType === 1 ? this.makeButton(arr1) : ''}
        {codeType === 2 ? this.makeButton(arr3) : ''}
        {codeType === 3 ? this.makeButton(arr2) : ''}
        <br />

        {codeType < 0 ? (
          <React.Fragment>
            <button className="btn-primary m-3" onClick={() => this.codeABC()}>
              Code:ABC123
            </button>
            <button className="btn-primary m-3" onClick={() => this.code0_9()}>
              Code:0-9
            </button>
            <button className="btn-primary m-3" onClick={() => this.codeMN()}>
              Code:MN0123
            </button>
          </React.Fragment>
        ) : (
          ''
        )}
        <br />
        <h3>List of Code Created</h3>
        <br />
        <ul>
          {newArr.map((st) => (
            <li>{st}</li>
          ))}
        </ul>
      </div>
    )
  }
}
export default Event
