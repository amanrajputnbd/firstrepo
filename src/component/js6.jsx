import React, { Component } from 'react'
class Post extends Component {
  state = {
    posts: [
      {
        postId: 255,
        heading: 'World Cup Semi-final',
        postedBy: 'Vishal',
        numOfLikes: 45,
        numOfShares: 10,
        txt: 'India lost to New Zealand in the world cup. Very Bad.',
      },
      {
        postId: 377,
        heading: 'What a final',
        postedBy: 'Mohit',
        numOfLikes: 18,
        numOfShares: 4,
        txt:
          'Was anyone awake to see the final. England won by boundary count.',
      },
      {
        postId: 391,
        heading: 'Was it 5 runs on 6 six runs',
        postedBy: 'Preeti',
        numOfLikes: 29,
        numOfShares: 7,
        txt:
          'I feed sorry for New Zealand. If the ball had not hit the bat and no overthrows, New Zealand would have won.',
      },
      {
        postId: 417,
        heading: 'Will Dhoni retire',
        postedBy: 'Hemant',
        numOfLikes: 66,
        numOfShares: 24,
        txt:
          "Is this Dhoni's final match. Will be ever see Dhoni playing for India.",
      },
    ],
  }
  handleLike = (id) => {
    let s1 = { ...this.state }
    let post = s1.posts.find((p1) => p1.postId === id)
    if (post) post.numOfLikes++
    this.setState(s1)
  }
  handleShare = (id) => {
    let s1 = { ...this.state }
    let post = s1.posts.find((p1) => p1.postId === id)
    if (post) post.numOfShares++
    this.setState(s1)
  }
  render() {
    const { posts } = this.state
    return (
      <div className="container">
        {posts.map((p1) => {
          const { heading, postedBy, numOfLikes, numOfShares, txt, postId } = p1
          return (
            <div className="row bg-light border p-2">
              <div className="col">
                <h5>{heading}</h5>
                shared By:{postedBy}
                {txt}
                <br />
                Likes:{numOfLikes}
                <button
                  className="btn btn-primary btn-sm m-1"
                  onClick={() => this.handleLike(postId)}
                >
                  Like
                </button>
                share:{numOfShares}
                <button
                  className="btn btn-primary btn-sm m-1"
                  onClick={() => this.handleShare(postId)}
                >
                  share
                </button>
              </div>
            </div>
          )
        })}
      </div>
    )
  }
}
export default Post
