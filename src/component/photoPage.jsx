import React, { Component } from 'react'
import PhotoComp from './photoComp'
class PhotoPage extends Component {
  state = {
    photos: [
      {
        img: 'https://picsum.photos/id/1032/240/160',
        like: 2,
        dislike: 9,
        myOption: 'dislike',
      },
      {
        img: 'https://picsum.photos/id/1051/240/160',
        like: 23,
        dislike: 8,
        myOption: 'like',
      },
      {
        img: 'https://picsum.photos/id/1050/240/160',
        like: 32,
        dislike: 12,
        myOption: '',
      },
      {
        img: 'https://picsum.photos/id/1031/240/160',
        like: 28,
        dislike: 6,
        myOption: '',
      },
      {
        img: 'https://picsum.photos/id/1079/240/160',
        like: 22,
        dislike: 5,
        myOption: 'dislike',
      },
      {
        img: 'https://picsum.photos/id/1080/240/160',
        like: 9,
        dislike: 2,
        myOption: 'like',
      },
    ],
  }

  handleLike = (index) => {
    let s1 = { ...this.state }
    let photo = s1.photos[index]
    let { like, dislike, myOption } = photo
    if (myOption === 'like') {
      like--
      myOption = ''
    } else if (myOption === 'dislike') {
      like++
      dislike--
      myOption = 'like'
    } else {
      like++
      myOption = 'like'
    }
    photo.like = like
    photo.dislike = dislike
    photo.myOption = myOption
    this.setState(s1)
  }
  handleDelete = (index) => {
    let s1 = { ...this.state }
    let photo = s1.photos.findIndex((p1, index) => index === index)
    s1.photos.splice(photo, 1)
    this.setState(s1)
  }
  handleDislike = (index) => {
    let s1 = { ...this.state }
    let photo = s1.photos[index]
    let { like, dislike, myOption } = photo
    if (myOption === 'dislike') {
      dislike--
      myOption = ''
    } else if (myOption === 'like') {
      like--
      dislike++
      myOption = 'dislike'
    } else {
      dislike++
      myOption = 'dislike'
    }
    photo.like = like
    photo.dislike = dislike
    photo.myOption = myOption
    this.setState(s1)
  }

  render() {
    const { photos } = this.state
    return (
      <div className="container">
        <div className="row text-center p-5">
          {photos.map((p1, index) => (
            <PhotoComp
              photo={p1}
              index={index}
              onLike={this.handleLike}
              onDislike={this.handleDislike}
              onDelete={this.handleDelete}
            />
          ))}
        </div>
      </div>
    )
  }
}
export default PhotoPage
